/****************************************Copyright (c)**************************************************                               
**                               Henan Star Hi-Tech CO.,LTD                                       
**                                  All rights reserved. 
** 
**----------------------------------------File Info----------------------------------------------------- 
** 文件名称： 
** 工程项目： 
** 说    明： 
**  
** 作    者：              日    期： 
** 建立版本：              
** 
**----------------------------------------modification-------------------------------------------------- 
** 作    者： 
** 日    期： 
** 版    本：            标    记： 
** 说    明： 
** 
********************************************************************************************************/ 
 
#ifndef __XYMODEM_H_ 
#define __XYMODEM_H_ 
 
#define MODEM_MAX_RETRIES   50      //接收等待延时时间 
#define MODEM_CRC_RETRIES   51      //>MODEM_MAX_RETRIES固定为CRC校验 
#define MODEM_CAN_COUNT     3       //Wait for 3 times CAN before quiting 
#define MODEM_EOT_COUNT     1 
 
#define MODEM_SOH  0x01        //数据块起始字符 
#define MODEM_STX  0x02 
#define MODEM_EOT  0x04 
#define MODEM_ACK  0x06 
#define MODEM_NAK  0x15 
#define MODEM_CAN  0x18 
#define MODEM_C    0x43 
 
typedef struct{ 
    int           modemtype; 
    int           crc_mode; 
    int           nxt_num;          //下一数据块序号 
    int           cur_num;          //当块序号 
    int           len; 
    int           rec_err;          //数据块接收状态 
    unsigned char buf[1024];        //数据 
    unsigned int  filelen;          //Ymodem可有带文件名称和长度 
    unsigned char filename[32]; 
}modem_struct; 
 
 
#ifdef __cplusplus 
    extern "C"{ 
#endif 
     
int ymodem_init(modem_struct *mblock); 
int modem_recvdata(modem_struct *mblock); 
//int crc_16(unsigned char *buf, int len); 
void modem_cancle(void); 
#ifdef __cplusplus 
    } 
#endif 
 
#endif 
