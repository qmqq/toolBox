#include "toolfun.h"
#include "QDebug"
#include "QMessageBox"
#include <QMainWindow>

QString ByteArrayToHexStr(QByteArray data)
{
    QString temp="";
    QString hex=data.toHex();
    for (int i=0;i<hex.length();i=i+2)
    {
        temp+=hex.mid(i,2)+" ";
    }
    return temp.trimmed().toUpper();
}

QByteArray strHexToByteArry(QString &HexString)
{
    bool ok;
    QByteArray ret;
    QString hexStr;
    HexString = HexString.remove(QRegExp("\\s")); //删除空格
    if(HexString.length()%2 !=0) //不足偶数位补足
    {
        HexString = "0" + HexString;
    }
    for(int i=0; i<HexString.length(); i+=2)
    {
        hexStr += HexString.mid(i,2) + " ";
    }
    hexStr = hexStr.trimmed();
    QStringList sl = hexStr.split(" ");
    foreach (QString s, sl)
    {
        if(!s.isEmpty())
        {
            char c = s.toInt(&ok,16)&0xFF;
            if(ok)
            {
                ret.append(c);
            }
            else
            {
                qDebug()<<"非法的16进制字符："<<s;
                QMessageBox::warning(0,QObject::tr("错误："),QString("非法的16进制字符: \"%1\"").arg(s));
                ret.clear();
                break;
            }
        }
    }
    HexString = hexStr;
    return ret;
}

QString GetCorrectUnicode(const QByteArray &ba)
{
    QTextCodec::ConverterState state;
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QString text = codec->toUnicode(ba.constData(), ba.size(), &state);
    if (state.invalidChars > 0)
    {
        text = QTextCodec::codecForName("GBK")->toUnicode(ba);
    }
    else
    {
        text = ba;
    }
    return text;
}
