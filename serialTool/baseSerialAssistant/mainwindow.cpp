#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include "QStringList"
#include "QDateTime"
#include "toolfun.h"
#include "QStringList"
#include "QMessageBox"
#include "QSettings"
#include "QFileDialog"
#include "QDataStream"
#include "QFile"
#include "dialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    recFreeTimer = new QTimer;
    autoSendTimer = new QTimer;
    ui->setupUi(this);

    MainWindow::connect(ui->action_startPort,SIGNAL(toggled(bool)),this,SLOT(actionPortCtrOpenButtonSlot(bool)));
    MainWindow::connect(ui->action_closePort,SIGNAL(triggered(bool)),this,SLOT(actionPortCtrCloseButtonSlot()));
    MainWindow::connect(ui->action_clearPort,SIGNAL(triggered(bool)),this,SLOT(actionPortCtrClearButtonSlot()));
    MainWindow::connect(&serial,SIGNAL(readyRead()),this,SLOT(reStartRecTimerSlot()));
    MainWindow::connect(recFreeTimer,SIGNAL(timeout()),this,SLOT(readyReadySlot()));
    MainWindow::connect(autoSendTimer,SIGNAL(timeout()),this,SLOT(buttonOpenSendButtonSlot()));

    MainWindow::connect(ui->openSendPushButton,SIGNAL(clicked(bool)),this,SLOT(buttonOpenSendButtonSlot()));
    MainWindow::connect(ui->historyComboBox,SIGNAL(activated(int)),this,SLOT(buttonHistoryButtonSlot()));
    MainWindow::connect(ui->action_stoneFile,SIGNAL(triggered(bool)),this,SLOT(actionStoneButtonSlot()));

    MainWindow::connect(ui->portComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->baudRateComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->stopBitComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->checkBitComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->dataBitComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->flowCtrComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(sendOptChangeSlot()));
    MainWindow::connect(ui->actionHex_Str,SIGNAL(triggered(bool)),this,SLOT(showHexTotextDialigSlot()));
    MainWindow::connect(ui->aboutAction,SIGNAL(triggered(bool)),this,SLOT(aboutActionSlot()));
    initSerial();

}

MainWindow::~MainWindow()
{
    delete ui;
}

 void MainWindow::upDateStatusBar()
{
    portStatuLable.setText(statusBarMsg);
    QString statuRTx;
    statuRTx = "Tx" + statuRTx.setNum(TxNum);
    sendNumLable.setText(statuRTx);
    statuRTx = "Rx" + statuRTx.setNum(RxNum);
    recNumLable.setText(statuRTx);
}

void MainWindow::initSerial(void)
{
    /*查找端口*/
    checkSerial();
    /*设置波特率、校验位、数据位、停止位comBox*/
    QStringList baudList; //波特率
    QStringList parityList; //校验位
    QStringList dataBitList; //数据位
    QStringList stopBitList; //停止位
    QStringList flowControlList; //流控
    baudList<<"1200"<<"2400"<<"4800"<<"9600"<<
              "19200"<<"38400"<<"57600"<<"115200";

    parityList<<"None"<<"Even"<<"Odd"<<"Space"<<"Mark";
    dataBitList<<"5"<<"6"<<"7"<<"8";
    stopBitList<<"1"<<"1.5"<<"2";
    flowControlList<<"none"<<"Hw"<<"Sw";
    ui->baudRateComboBox->clear();
    ui->checkBitComboBox->clear();
    ui->dataBitComboBox->clear();
    ui->stopBitComboBox->clear();
    ui->baudRateComboBox->addItems(baudList);
    ui->checkBitComboBox->addItems(parityList);
    ui->dataBitComboBox->addItems(dataBitList);
    ui->stopBitComboBox->addItems(stopBitList);
    ui->flowCtrComboBox->addItems(flowControlList);
    /*设置其他界面状态*/
    ui->openSendPushButton->setText("打开");
    ui->action_startPort->setCheckable(true);

    ui->timeOutSpinBox->setMaximum(1000);
    ui->timeOutSpinBox->setMinimum(4);
    ui->timeOutSpinBox->setValue(20);

    ui->autoSendSpinBox->setMaximum(10000);
    ui->autoSendSpinBox->setMinimum(10);
    ui->autoSendSpinBox->setValue(1000);

    RxNum = 0;
    TxNum = 0;
    statusBarMsg = "CLOSED";
    upDateStatusBar();
    ui->statusBar->addWidget(&portStatuLable);
    ui->statusBar->addWidget(&sendNumLable);
    ui->statusBar->addWidget(&recNumLable);
    ui->serialRecTextEdit->setReadOnly(true);

    ui->asciiRadioButton->setChecked(true);
    ui->asciiRadioButton_2->setChecked(true);
    readSet();
}

void MainWindow::checkSerial(void)
{
    ui->portComboBox->clear();
    /*查找端口*/
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        qDebug()<<"Name:"<<info.portName();
        ui->portComboBox->addItem(info.portName());
        qDebug()<<"Description:"<<info.description();
        qDebug()<<"Manufacture:"<<info.manufacturer();
    }
}

void MainWindow::on_action_showToolBar_triggered()
{
    ui->mainToolBar->show();
}


void MainWindow::actionPortCtrOpenButtonSlot(bool check)
{
    if(check)
    {
        openSerial();
        ui->action_closePort->setChecked(false);
    }
    else
    {
        closeSerial();
    }

}

void MainWindow::actionPortCtrCloseButtonSlot()
{
    closeSerial();
    ui->action_startPort->setChecked(false);
}

void MainWindow::actionPortCtrClearButtonSlot()
{
    clearSerial();
}

void MainWindow::readyReadySlot()
{
    if(ui->disTimeCheckBox->isChecked())
    {
        timeSerial();
    }
    readSerial();
    if(ui->autoWrapCheckBox->isChecked())
    {
        ui->serialRecTextEdit->insertPlainText("\r\n");
    }
    ui->serialRecTextEdit->moveCursor(QTextCursor::End);
}

void MainWindow::reStartRecTimerSlot()
{
    recFreeTimer->start(ui->timeOutSpinBox->value());
    data += serial.readAll();
}

void MainWindow::buttonOpenSendButtonSlot()
{
    if(serial.isOpen())
    {
        autoSendTimer->stop();
        if(sendSerial())
        {
            QString str = ui->sendTextEdit->document()->toPlainText();
            /*设置历史消息窗口*/
            if((ui->historyComboBox->currentText() != str) && !str.isEmpty())
            {
                ui->historyComboBox->addItem(str);
                ui->historyComboBox->setCurrentIndex(ui->historyComboBox->count()-1);
            }
            /*显示发送*/
            if(ui->disSendCheckBox->isChecked())
            {
                if(ui->disTimeCheckBox->isChecked())
                {
                    timeSerial();
                }
                ui->serialRecTextEdit->insertPlainText(str);
                if(ui->autoWrapCheckBox->isChecked())
                {
                    ui->serialRecTextEdit->insertPlainText("\r\n");
                }
                ui->serialRecTextEdit->moveCursor(QTextCursor::End);
            }
            /*自动发送*/
            if(ui->autoSendCheckBox->isChecked())
            {
                autoSendTimer->start(ui->autoSendSpinBox->value());
            }
            /*计算和跟新显示状态栏Tx数目*/
        }
    }
    else
    {
        ui->action_startPort->setChecked(true);
    }
}

void MainWindow::buttonHistoryButtonSlot()
{
    QString str = ui->historyComboBox->currentText();
    ui->sendTextEdit->clear();
    ui->sendTextEdit->append(str);
}

void MainWindow::openSerial()
{
    int time = 20;
    serial.setPortName(ui->portComboBox->currentText());
    switch(ui->baudRateComboBox->currentIndex())
    {
    case 0:
        serial.setBaudRate(QSerialPort::Baud1200);
        time = 300;
        break;
    case 1:
        serial.setBaudRate(QSerialPort::Baud2400);
        time = 150;
        break;
    case 2:
        serial.setBaudRate(QSerialPort::Baud4800);
        time = 75;
        break;
    case 3:
        serial.setBaudRate(QSerialPort::Baud9600);
        time = 36;
        break;
    case 4:
        serial.setBaudRate(QSerialPort::Baud19200);
        time = 20;
        break;
    case 5:
        serial.setBaudRate(QSerialPort::Baud38400);
        time = 10;
        break;
    case 6:
        serial.setBaudRate(QSerialPort::Baud57600);
        time = 10;
        break;
    case 7:
        serial.setBaudRate(QSerialPort::Baud115200);
        time = 5;
        break;
    }

    switch(ui->dataBitComboBox->currentText().toInt())
    {
    case 5:
        serial.setDataBits(QSerialPort::Data5);
        break;
    case 6:
        serial.setDataBits(QSerialPort::Data6);
        break;
    case 7:
        serial.setDataBits(QSerialPort::Data7);
        break;
    case 8:
        serial.setDataBits(QSerialPort::Data8);
        break;
    }

    switch(ui->checkBitComboBox->currentIndex())
    {
    case 0:
        serial.setParity(QSerialPort::NoParity);
        break;
    case 1:
        serial.setParity(QSerialPort::EvenParity);
        break;
    case 2:
        serial.setParity(QSerialPort::OddParity);
        break;
    case 3:
        serial.setParity(QSerialPort::SpaceParity);
        break;
    case 4:
        serial.setParity(QSerialPort::MarkParity);
        break;
    }

    switch(ui->stopBitComboBox->currentIndex())
    {
    case 0:
        serial.setStopBits(QSerialPort::OneStop);
        break;
    case 1:
        serial.setStopBits(QSerialPort::OneAndHalfStop);
        break;
    case 2:
        serial.setStopBits(QSerialPort::TwoStop);
        break;
    }

    switch(ui->flowCtrComboBox->currentIndex())
    {
    case 0:
        serial.setFlowControl(QSerialPort::NoFlowControl);
        break;
    case 1:
        serial.setFlowControl(QSerialPort::HardwareControl);
        break;
    case 2:
        serial.setFlowControl(QSerialPort::SoftwareControl);
        break;
    }


    if(serial.open(QIODevice::ReadWrite))
    {
        ui->timeOutSpinBox->setValue(time);
        ui->action_closePort->setChecked(false);
        QString msg;
        statusBarMsg = "OPENED   " + ui->portComboBox->currentText()+"   " + ui->baudRateComboBox->currentText();
        RxNum = 0;
        TxNum = 0;
        upDateStatusBar();
        ui->openSendPushButton->setText("发送");
    }
    else
    {
        qDebug()<<"打开串口出错";
        QMessageBox::warning(this,"错误","串口被占用或不存在");
        ui->action_startPort->setChecked(false);
    }
}

void MainWindow::readSerial()
{
    if(!data.isEmpty())
    {
        QString str;
        if(ui->hexRadioButton->isChecked())
        {
            str = ByteArrayToHexStr(data);
            str+=" ";
        }
        else
        {
            str = GetCorrectUnicode(data);
        }
        ui->serialRecTextEdit->insertPlainText(str);
        RxNum += data.length();
        upDateStatusBar();
    }
    recFreeTimer->stop();
    data.clear();
}

bool MainWindow::sendSerial()
{
    QString str;
    QByteArray sendBuf;
    str = ui->sendTextEdit->document()->toPlainText();

    if(!str.isEmpty())
    {
        if(ui->hexRadioButton_2->isChecked())
        {
            sendBuf = strHexToByteArry(str);
            ui->sendTextEdit->clear();
            ui->sendTextEdit->append(str);
        }
        else
        {
            sendBuf = str.toLocal8Bit();
        }
        TxNum += sendBuf.length();
        upDateStatusBar();
    }
    if(!sendBuf.isEmpty())
    {
        serial.write(sendBuf);
        return true;
    }
    return false;
}

void MainWindow::closeSerial()
{
    serial.close();
    ui->openSendPushButton->setText("打开");
    statusBarMsg = "CLOSED";
    RxNum = 0;
    TxNum = 0;
    ui->action_startPort->setChecked(false);
    upDateStatusBar();
}

void MainWindow::clearSerial()
{
    if(serial.isOpen())
    {
        serial.clear(QSerialPort::AllDirections);
    }
    ui->serialRecTextEdit->clear();
    RxNum = 0;
    TxNum = 0;
    upDateStatusBar();
}

void MainWindow::timeSerial()
{
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("hh:mm:ss.zzz");
    current_date = "[" + current_date + "]";
    ui->serialRecTextEdit->insertPlainText(current_date);
}

void MainWindow::writeSet()
{
    QSettings settings("Qmqq","QSerial");
    /*存储发送设置*/
    settings.setValue("baudRate",ui->baudRateComboBox->currentIndex());
    settings.setValue("dataBit",ui->dataBitComboBox->currentIndex());
    settings.setValue("checkBit",ui->checkBitComboBox->currentIndex());
    settings.setValue("stopBit",ui->stopBitComboBox->currentIndex());
    settings.setValue("flowCtr",ui->flowCtrComboBox->currentIndex());

    /*存储接收设置*/
    settings.setValue("ascii",ui->asciiRadioButton->isChecked());
    settings.setValue("hex",ui->hexRadioButton->isChecked());

    /*存储发送设置*/
    settings.setValue("ascii_2",ui->asciiRadioButton_2->isChecked());
    settings.setValue("hex_2",ui->hexRadioButton_2->isChecked());

}

void MainWindow::readSet()
{
    QSettings settings("Qmqq","QSerial");
    /*读取发送设置*/
    ui->baudRateComboBox->setCurrentIndex(settings.value("baudRate",1).toInt());
    ui->dataBitComboBox->setCurrentIndex(settings.value("dataBit",1).toInt());
    ui->checkBitComboBox->setCurrentIndex(settings.value("checkBit",1).toInt());
    ui->stopBitComboBox->setCurrentIndex(settings.value("stopBit",1).toInt());
    ui->flowCtrComboBox->setCurrentIndex(settings.value("flowCtr",1).toInt());

    /*读取接收设置*/
     ui->asciiRadioButton->setChecked(settings.value("ascii",false).toBool());
     ui->hexRadioButton->setChecked(settings.value("hex",false).toBool());

     /*读取发送设置*/
     ui->asciiRadioButton_2->setChecked(settings.value("ascii_2",false).toBool());
     ui->hexRadioButton_2->setChecked(settings.value("hex_2",false).toBool());
}

void MainWindow::actionStoneButtonSlot()
{
    qDebug()<<"here";
    QString stoneFileName = QFileDialog::getSaveFileName(this, tr("Save File"),".",tr("Txt (*.txt)"));
    if(!stoneFileName.isEmpty())
    {
        QFile *stoneFile = new QFile(stoneFileName);
        stoneFile->open(QIODevice::ReadWrite);
        QString str = ui->serialRecTextEdit->document()->toPlainText();
        stoneFile->write(str.toLocal8Bit());
        stoneFile->close();
    }
}

void MainWindow::sendOptChangeSlot()
{
    if(serial.isOpen())
    {
        serial.close();
        openSerial();
    }
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType);
    Q_UNUSED(result);
    MSG *msg = reinterpret_cast<MSG*>(message);
    int msgType = msg->message;
    if(msgType == WM_DEVICECHANGE)
    {
        closeSerial();
        checkSerial();
    }
    return false;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSet();
    qDebug()<<"exit";
    hexToTextDialog.close();
    aboutDialog.close();
    event->accept();
}

void MainWindow::showHexTotextDialigSlot()
{
    hexToTextDialog.setWindowTitle("Conversion");
    hexToTextDialog.show();
}

void MainWindow::aboutActionSlot()
{
    aboutDialog.setWindowTitle("About");
    aboutDialog.show();
}
