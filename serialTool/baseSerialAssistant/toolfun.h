#ifndef __TOOL_FUN_H_
#define __TOOL_FUN_H_

#include "QString"
#include "QByteArray"
#include "QTextCodec"

QString ByteArrayToHexStr(QByteArray data);
QString GetCorrectUnicode(const QByteArray &ba);
QByteArray strHexToByteArry(QString &HexString);
#endif
