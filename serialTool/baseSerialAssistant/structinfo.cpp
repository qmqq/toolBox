#if 0
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
#define STRUCT_NAME_MAX_SIZE 	15
#define STRUCT_CLASS_MAX_NUM	20

typedef enum
{
    __CHAR,
    __INT,
    __FLOAT,
}InfoType;

typedef struct
{
    char 			name[STRUCT_NAME_MAX_SIZE];
    InfoType 		_type;
    unsigned  char 	len;
    int* 	 		Val_I;
    float* 	 		Val_F;
    char*			Val_C;
}_STRUCT_INFO;

typedef struct
{
    char 			msg[20];
    unsigned char 	len;
    _STRUCT_INFO 	arryInfo[STRUCT_CLASS_MAX_NUM];
}ShowStruct;

void structShow(ShowStruct* _show,void* source)
{
    unsigned char i,j;
    char *p = source;
    printf("%s:\r\n",_show->msg);
    for(i = 0; i<_show->len; i++)
    {
        printf("%-10s",_show->arryInfo[i].name);
        switch(_show->arryInfo[i]._type)
        {

            case __INT:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                    {
                        printf("%d ",*(int*)p);
                        p +=4;
                    }
                printf("\r\n");
                break;
            case __FLOAT:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                    {
                        printf("%f ",*(float*)p);
                        p +=4;
                    }
                printf("\r\n");
                break;
            case __CHAR:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                    {
                        printf("%c",*(char*)p);
                        p++;
                    }
                printf("\r\n");
                break;
        }
    }
    printf("\r\n");
}

typedef struct
{
    int   a[3];
    float b[4];
    char  c;
    char  d;
    char  e[12];
}Demo;

Demo demo = {{10,12,13},{15.0,13.0,15.0,16.0},'a','b',"defdf"};

ShowStruct show =
{
    "nihaoya",5,
    {
        {"name"	,__INT	, 3},
        {"char"	,__FLOAT, 4},
        {"ok"	,__CHAR	, 1},
        {"nen"	,__CHAR	, 1},
        {"nen"	,__CHAR	, 4},
    }
};

int main(int argc, char *argv[]) {
    demo.a[0] = 123;
    structShow(&show,&demo);
    return 0;
}
#endif
