#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T10:17:15
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = baseSerialAssistant
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    toolfun.cpp \
    dialog.cpp \
    about.cpp \
    structinfo.cpp \
    frameanalyzedialog.cpp

HEADERS  += mainwindow.h \
    toolfun.h \
    dialog.h \
    about.h \
    frameanalyzedialog.h

FORMS    += mainwindow.ui \
    dialog.ui \
    about.ui \
    frameanalyzedialog.ui

RESOURCES += \
    image.qrc
RC_ICONS = Image\app.ico
