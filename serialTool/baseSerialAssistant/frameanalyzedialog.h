#ifndef FRAMEANALYZEDIALOG_H
#define FRAMEANALYZEDIALOG_H

#include <QDialog>

namespace Ui {
class frameAnalyzeDialog;
}

class frameAnalyzeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit frameAnalyzeDialog(QWidget *parent = 0);
    ~frameAnalyzeDialog();

private:
    Ui::frameAnalyzeDialog *ui;
};

#endif // FRAMEANALYZEDIALOG_H
