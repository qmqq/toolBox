#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>
#include <QCloseEvent>
#include <windows.h>
#include "dialog.h"
#include "about.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_action_showToolBar_triggered();
    void actionPortCtrOpenButtonSlot(bool check);
    void actionPortCtrCloseButtonSlot(void);
    void actionPortCtrClearButtonSlot(void);
    void buttonOpenSendButtonSlot(void);
    void buttonHistoryButtonSlot(void);
    void readyReadySlot(void);
    void reStartRecTimerSlot(void);
    void actionStoneButtonSlot(void);
    void sendOptChangeSlot(void);
    void showHexTotextDialigSlot();
    void aboutActionSlot();

private:
    Ui::MainWindow *ui;
    QLabel portStatuLable;
    QLabel recNumLable;
    QLabel sendNumLable;
    QSerialPort serial;
    QTimer *recFreeTimer;
    QTimer *autoSendTimer;
    QByteArray data;
    Dialog hexToTextDialog;
    about aboutDialog;
    int TxNum;
    int RxNum;
    QString statusBarMsg;
    QObjectUserData setData;
    void upDateStatusBar(void);
    void openSerial(void);
    void closeSerial(void);
    void clearSerial(void);
    void readSerial(void);
    bool sendSerial(void);
    void timeSerial(void);
    void initSerial(void); //初始化界面和串口
    void checkSerial(void); //查找到新的串口并修改界面
    void writeSet(void);
    void readSet(void);

protected:
    void closeEvent(QCloseEvent *event) override;
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
};

#endif // MAINWINDOW_H
