#include "dialog.h"
#include "ui_dialog.h"
#include "toolfun.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    this->connect(ui->toHexPushButton,SIGNAL(clicked(bool)),this,SLOT(toHexSlot()));
    this->connect(ui->toTextPushButton,SIGNAL(clicked(bool)),this,SLOT(toTextSlot()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::toHexSlot()
{

    QString str;
    str = ui->textTextEdit_2->document()->toPlainText(); //读取字符串
    str = ByteArrayToHexStr(str.toUtf8()); //转换
    ui->hexTextEdit->clear();
    ui->hexTextEdit->append(str); //填写数据

}

void Dialog::toTextSlot()
{
    QString str;
    QByteArray arry;
    str = ui->hexTextEdit->document()->toPlainText(); //读取字符串
    arry = strHexToByteArry(str); //字符串转换为16进制
    ui->hexTextEdit->clear();
    ui->hexTextEdit->append(str);
    ui->textTextEdit_2->clear();
    ui->textTextEdit_2->append(QString::fromUtf8(arry));
}
