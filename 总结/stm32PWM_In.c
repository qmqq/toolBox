#define PWM_IN_TIM_FREQ    (1000000)
#define ENABLE_PWM_IN_CCR1 (1)
#define ENABLE_PWM_IN_CCR2 (1)
#define ENABLE_PWM_IN_CCR3 (1)
#define ENABLE_PWM_IN_CCR4 (1)

const uint16_t  TIM_IT_CCRx   [4] = {TIM_IT_CC1,TIM_IT_CC2,TIM_IT_CC3,TIM_IT_CC4};

typedef struct
{
    int hightTime;
    int lowTime;
    float    dutyCycle;
    float    frequency;
}PWM_ValType; /*用于存储计算后的PWM数据*/

typedef struct
{
    uint32_t CCx[4][4];    /*记录四个通道，四个点的时间*/
    uint32_t OVFLOW[4][4]; /*记录四个通道，四个点的溢出计数器的值，每溢出一次其值增加TIMx->ARR的值*/
    uint8_t  newData[4];   /*获取到一个新的频率数据*/
}PWM_StatuType; /*用于中断函数中记录PWM输入的时间特性，用户不需要访问*/

/*
捕获正常
T：周期
H：高电平时间
L：低电平时间
    点1            点2      点3     点4
     |------|        |-------|       |------
-----|      |--------|       |-------|

     |               |       |       |
     |               |       |       |
     |<------T------>|<--H-->|<--L-->|
T = T;
H = H;
L = L;

占空比太小，高电平时间太少
    点1       点2           点3   点4
     |-|        |-|       |-|       |-|
-----| |--------| |-------| |-------| |-------

     |          |           |       |
     |          |           |       |
     |<---T---->|<----H---->|<--L-->|
T = T;
H = (H-L)/2 或者 H%T
L = L 或者 L = T-H;

占空比太高，低电平时间太少
    点1     点2   点3         点4
     |----|  |-----|  |-----|  |-----|
-----|    |--|     |--|     |--|     |-------

     |       |     |           |
     |       |     |           |
     |<--T-->|<-H->|<----L---->|
T = T;
H = H 或者 T-H
L = (L-H)/2 或者 L%T 
     
*/

PWM_StatuType TIMx_statu;
/*定时器中断函数*/
void TIM2_IRQHandler(void)
{
    static TIM_TypeDef*    const TIMx = TIM2; /*填写当前的定时器号*/
    static PWM_StatuType*  const pwmInVal = &TIMx_statu; /*填写指定的定时器数据存储区*/
    static uint32_t        overFlowCount = 0; /*定时器溢出计数，每次溢出会自增TIMx->ARR的值*/
    static uint8_t         next[4] = {0};
    static uint8_t  ccrx = 0;
    
    if(TIM_GetITStatus(TIMx, TIM_IT_Update) != RESET)
    {     
        overFlowCount+=TIMx->ARR;
        TIM_ClearITPendingBit(TIMx, TIM_IT_Update);
    }
#if ENABLE_PWM_IN_CCR1  
    ccrx = 0; 
    if( (TIMx->SR & *(TIM_IT_CCRx+ccrx)) && (TIMx->DIER & *(TIM_IT_CCRx+ccrx)) )//传感器1中断
    {
        //TIM_ClearITPendingBit(TIMx, TIM_IT_CC1); //清除中断标志位 
        TIMx->SR &= ~*(TIM_IT_CCRx+ccrx);
        switch(next[ccrx])
        {
            case 0:
                next[ccrx] = 1; /*捕获上升沿*/
                pwmInVal->CCx[ccrx][0] = *TIM_CCRx[ccrx];
                pwmInVal->OVFLOW[ccrx][0] = overFlowCount;
                break;
            case 1:
                next[ccrx] = 2; /*捕获上升沿，获得周期*/
                pwmInVal->CCx[ccrx][1] = *TIM_CCRx[ccrx];
                pwmInVal->OVFLOW[ccrx][1] = overFlowCount;
                //TIM_OC1PolarityConfig(TIMx, TIM_OCPolarity_Low); /*设置下降沿触发*/
                TIMx->CCER |= (TIM_OCPolarity_Low<<(ccrx<<2)); /*寄存器操作*/  

                break;
            case 2:
                next[ccrx] = 3; /*捕获下降沿*/
                pwmInVal->CCx[ccrx][2] = *TIM_CCRx[ccrx];
                pwmInVal->OVFLOW[ccrx][2] = overFlowCount;
                //TIM_OC1PolarityConfig(TIMx, TIM_OCPolarity_High); /*设置上升沿触发*/
                TIMx->CCER &= (uint16_t)~(TIM_OCPolarity_Low<<(ccrx<<2)); /*寄存器操作*/ 
                break;
            case 3:
                next[ccrx] = 0; /*结束捕获*/
                pwmInVal->CCx[ccrx][3] = *TIM_CCRx[ccrx];
                pwmInVal->OVFLOW[ccrx][3] = overFlowCount;
                TIM_ITConfig(TIMx,TIM_IT_CC1,DISABLE); /*CC1检测中断关闭*/
                //TIMx->DIER &= (uint16_t)~TIM_IT_CCRx[ccrx]; /*寄存器操作*/
                pwmInVal->newData[ccrx]++;
            break;
        }
    } 
#endif
}

uint32_t overFlowCheck(uint32_t firstTime, uint32_t nextTime)
{
    uint32_t ret = 0;
    if(nextTime < firstTime)
    {
        ret = 0xFFFFFFFF-firstTime+nextTime;
    }
    else
    {
        ret = nextTime - firstTime;
    }
    return ret;
}

/*返回0没有获取到PWM脉宽，返回1获取到PWM脉宽*/
uint8_t TIMx_PWM_InRead(uint8_t ch, __IO PWM_ValType *pwmRetVal, uint16_t reTry)
{
    static          PWM_StatuType* const pwmInVal = &TIMx_statu;
    uint32_t        overTime;
    uint32_t        cycleTick;
    static uint16_t repeat = 0; /*重复执行的次数*/
    static uint8_t  executeStatus = 0; /*执行状态*/
    uint8_t         ret = 0;
    
    switch(executeStatus)
    {
        
        case 0: /*开始检测PWM*/
            pwmInVal->newData[ch] = 0;
            repeat = 0;
            TIM2->SR &= ~TIM_IT_CCRx[ch];
            TIM2->DIER |= (uint16_t)TIM_IT_CCRx[ch]; /*开启中断*/
            executeStatus = 1;
            //break; 这里不需要break
        case 1: /*等待PWM检测完成*/
            if(pwmInVal->newData[ch] ==1 )
            {
                executeStatus = 2;
            }
            else
            {
                if(repeat>reTry) /*长时间未检测到PWM脉冲*/
                {
                    /*读取当前IO口的电平，判断占空比%100还是%0*/
                    
                }
                repeat++;
                break;
            }
        case 2: /*计算PWM数据,四组数据中数据1和数据2可以计算一个周期*/
            
            /*计算周期脉冲数1*/
            overTime = overFlowCheck(pwmInVal->OVFLOW[ch][0], pwmInVal->OVFLOW[ch][1]);
            cycleTick = overTime+pwmInVal->CCx[ch][1]- pwmInVal->CCx[ch][0];
        
            /*计算高电平*/
            overTime = overFlowCheck(pwmInVal->OVFLOW[ch][1], pwmInVal->OVFLOW[ch][2]);
            pwmRetVal->hightTime = overTime+pwmInVal->CCx[ch][2]- pwmInVal->CCx[ch][1];
            
            /*计算低电平*/
            overTime = overFlowCheck(pwmInVal->OVFLOW[ch][2], pwmInVal->OVFLOW[ch][3]);
            pwmRetVal->lowTime = overTime+pwmInVal->CCx[ch][3]- pwmInVal->CCx[ch][2];
            
            /*矫正高低电平时间*/
            if((pwmRetVal->hightTime + pwmRetVal->lowTime) > (int)(cycleTick*1.5) )
            {
                if((pwmRetVal->hightTime) >= (pwmRetVal->lowTime))
                {
                    pwmRetVal->hightTime = (pwmRetVal->hightTime-pwmRetVal->lowTime)/2;
                }
                else
                {
                    pwmRetVal->lowTime = (pwmRetVal->lowTime - pwmRetVal->hightTime)/2;
                }
                
            }
            /*计算占空比*/
            pwmRetVal->dutyCycle = (float)pwmRetVal->hightTime/(pwmRetVal->hightTime + pwmRetVal->lowTime);
            
            /*计算频率*/
            pwmRetVal->frequency = (float)PWM_IN_TIM_FREQ/cycleTick;
            executeStatus = 0;
            ret = 1;
            break;
    }
    return ret;
}
