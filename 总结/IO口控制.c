控制IO口从电平0至电平1之间，插入PWM上升沿过程，总时间60*30 tick

typedef struct
{
	uint8_t pulses; /*记录脉冲个数*/
	uint8_t clock;  /*记录时钟数*/ 
	uint8_t edge;   /*跳变沿*/
	uint8_t *GPIO;  /*IO*/ 
}SlopePinType;

uint8_t gpio =1;

SlopePinType g_slopPin = {0,0,0,&gpio};

void startSlopeCtr(SlopePinType *slopPin)
{
	*slopPin->GPIO = 0;
	slopPin->clock = 0;
	slopPin->edge = 0;
	/*开始上升沿解算*/
	slopPin->pulses = 0;
}

void slopeCtrIOProsess(SlopePinType *slopPin)
{
	if(slopPin->pulses < 30)
	{
		if(slopPin->clock == 60)
		{
			*slopPin->GPIO = 1;
			slopPin->clock = 0;
			slopPin->pulses++;
			slopPin->edge+=2;
		}
		else
		{
			if(slopPin->clock == slopPin->edge)
			{
				*slopPin->GPIO = 0;
			}
		}
		slopPin->clock++;
		printf("count=%d GPIO=%d edge=%d\r\n",slopPin->clock, *slopPin->GPIO, slopPin->edge);
	}
}


int main(int argc, char *argv[]) 
{
	while(1)
	{
		Sleep(1);
		slopeCtrIOProsess(&g_slopPin);
	}
	return 0;
}

/*标准普通io操作*/
typedef enum
{
   PinMode_In,
   PinMode_InUp,
   PinMode_InDw,
   PinMode_Out,
   PinMode_OutPP,
}PinModeType;

uint32_t getPortRcc(uint32_t port)
{
    
}

void pinMode(uint32_t port, uint32_t pin, PinModeType mode)
{
    GPIO_TypeDef* GPIOx=port;
    uint32_t rcc = getPortRcc(GPIOx);
    
}