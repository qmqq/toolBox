1. switch控制，帧接收，或用来减少整个流程延时

uint8_t processCtrl = 0;
while(1)
{
    switch(processCtrl)
    {
        case 0:
        //流程1
        if(xxx)
        {
            processCtrl = x; //条件符合跳转到流程x
        }
        break;
        case ...
        ...
        break;
        case x:
        ...
        break;
        default:
        ...
        break
    }
}

2.do{...}while(0)控制,代替goto

uint8_t errCode = 0;
do
{
    fun1();
    ...
    if(err1)
    {
        errCode = 1;
        break;
    }
    fun2();
    ...
    if(err2)
    {
        errCode = 2;
        break;
    }
    ...
}while(0);
errCodeProcess(errCode)
{
    ...
}

