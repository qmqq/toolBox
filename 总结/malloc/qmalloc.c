#include "qmalloc.h"
#include "malloc.h"
#include "stdio.h"

QmemType *_qmallocInit(char* data, int memSize, char dataTypeSize)
{
    int i;
    QmemType *mem = (QmemType*)malloc(sizeof(QmemType));
    if(mem != NULL)
    {
        mem->lave = memSize;
        mem->capacity = memSize;
        mem->free.next = (MemNodeType*)malloc(sizeof(MemNodeType)*memSize);
        if(mem->free.next != NULL)
        {
            MemNodeType* node = mem->free.next;
            for(i=0; i<memSize; i++)
            {
                node->owner = NULL;
                node->Previous = (data+i*dataTypeSize);
                node->next = &mem->free.next[i+1];
                node = node->next;
            }
            node->next = NULL;
            return mem;
        }
    }
    return NULL;
}

MemNodeType* _qmalloc(QmemType *mem)
{
    MemNodeType *ret = NULL;
    if(mem != NULL)
    {
        if(mem->lave>0) /*����ʣ��*/
        {
            ret = mem->free.next;
            mem->free.next = ret->next;
            ret->owner = mem;
            mem->lave--;
        }
    }
    return ret;
}

void _qfree(MemNodeType* node)
{
    if(node != NULL)
    {
        QmemType *mem = node->owner;
        if(mem != NULL)
        {
            if(mem->lave < mem->capacity)
            {
                node->next = mem->free.next;
                mem->free.next = node;
                node->owner = NULL;
                mem->lave++;
            }
        }
    }
}
