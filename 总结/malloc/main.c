#include <stdio.h>
#include <stdlib.h>
#include "qmalloc.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct
{
	char name[10];
	int val;
}TestType;

TestType buffer[3] = {{"nihao",10},{"buhao",50},{"douhhh",20}};

int main(int argc, char *argv[]) 
{
	MemNodeType* node;
	QmemType* mem;
	
	mem = qmallocinit(buffer, 3, TestType);
	
	node = qmalloc(mem);
	node = qmalloc(mem);
	
	qfree(node);
	node = qmalloc(mem);
	printf("%s\r\n", ((TestType*)node->Previous)->name);
	
	return 0;
}
