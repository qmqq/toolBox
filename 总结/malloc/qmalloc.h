#ifndef __QMALLOC_H_
#define __QMALLOC_H_


typedef struct _memNode
{
    struct   _memNode *next; 
    void     *owner; //所属
    void     *Previous;//容器
}MemNodeType;

typedef struct
{
    int capacity;
    int lave;
    MemNodeType free;
}QmemType;


QmemType *_qmallocInit(char* data, int memSize, char dataTypeSize);
MemNodeType* _qmalloc(QmemType *mem);
void _qfree(MemNodeType* node);

/*
例程：

typedef struct
{
	char name[10];
	int val;
}TestType;

TestType buffer[3]; //要管理的数组 

int main(void) 
{
	MemNodeType* node; //节点指针，用于存储malloc到的指针 
	QmemType* mem; //被管理的数据的表头，用于malloc的参数 
	mem = qmallocinit(buffer, 3, TestType); //初始化，buffer：被管理的数组指针， 2：数组的长度10， TestType：数组的类型 
	node = qmalloc(mem); //malloc到的数据 
	qfree(node); //释放数据 
	node = qmalloc(mem); //再次malloc 
	return 0;
}
*/
#define qmallocinit(data, dataLen, dataType) _qmallocInit((char*)data, dataLen, sizeof(dataType))
#define qmalloc(mem)                         _qmalloc(mem)
#define qfree(node)                          _qfree(node)

#endif /*__QMALLOC_H_*/
