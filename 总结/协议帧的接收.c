/**
 *	函 数 名:  reciveFrame(uint8_t _byte)
 *	功能说明:  接收帧数据的回调函数
 *	形    参:  _byte 串口接收到的字符
 *	返 回 值:  无
**/

void reciveFrame(uint8_t _byte)
{
    static uint8_t count = 0;
    static uint8_t frameLength = 0;
    static uint8_t buffer[MAX_FRAME_BUFFER_LENGTH] = {0};
	
    /*判断帧头*/
    switch(count)
    {
        case 0: //帧头
            if(_byte == 0x7E)
            {
                buffer[count++] = _byte;
            }
            break;
        case 1: //帧头
            if(_byte == 0xAA)
            {
                buffer[count++] = _byte;
            }
            else
            {
                count = 0;
            }
            break;
        case 2: //帧ID
            buffer[count++] = _byte;
            break;
        case 3: //获取帧长度
            frameLength = _byte+3; //3是帧头和校验和的总字节数
            if((frameLength>MAX_FRAME_BUFFER_LENGTH) || (frameLength==0))
            {
                frameLength = 0;
                count = 0;
            }
            else
            {
                buffer[count++] = _byte;
            }
            break;
        default: //帧内容
            if(count>3)
            {
                buffer[count++] = _byte;
                if(count ==  frameLength) //获取到完整帧
                {
                    inFrameQueue(&kz2Frame,buffer, count);
                    frameLength = 0;
                    count = 0; //可以继续接收下一帧
                }
            }
            break;
    }
}