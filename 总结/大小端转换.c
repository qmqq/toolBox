/* 大小端转换 */
#define LtoBs(s)	((s&0xff<<8)|(s&0xff00)>>8) /*小端转大端16位*/
#define BtoLs(s)	LtoBs(s) /*大端转小端16位*/

/*小端转大端32位*/
#define LtoBl(s)	((((uint32)(A) & 0xff000000) >> 24)| \
					 (((uint32)(A) & 0x00ff0000) >> 8) | \
				     (((uint32)(A) & 0x0000ff00) << 8) | \
				     (((uint32)(A) & 0x000000ff) << 24))
/*大端转小端32位*/
#define BtoLl(s)	LtoBl(s)