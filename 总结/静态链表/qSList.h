#ifndef __QSLIST_H
#define __QSLIST_H

#include "bsp.h"

typedef struct _qSListNode
{
    struct _qSListNode  *next; /*链表的下一个结点*/
    struct _qSListNode  *prev; /*链表的前一个结点*/
    int    weight;  /*结点的重量，用于大小排序*/
    int    nodeData;/*结点数据Container(容器)*/
    void*  own;
}QSListNode;

typedef struct
{
    int    weight;  /*结点的重量，用于大小排序*/
    int    nodeData;/*结点数据*/
}QSListItem;

typedef struct
{
    uint16_t capacity; /*链表空间大小*/
    uint16_t length; /*数据链表长度*/
    QSListNode *head; /*数据链表表头*/
    QSListNode *free; /*空闲链表表头,空闲结点的prev指针为NULL,*/
}QSList;

uint8_t qSListInit(QSList* list, QSListNode* nodeBuf,uint16_t nodeNum);
void qSListDelete(QSListNode* node);
QSListNode* qSListInsertPos(QSList* list, QSListItem* item, uint16_t pos);
QSListNode* qSListInsertMax(QSList* list, QSListItem* item);
void qSListClear(QSList* list);
uint16_t qSListLength(QSList* list);
uint16_t qSListCapacity(QSList* list);
QSListNode* qSListGet(QSList* list, uint16_t pos);

#endif /*__QSTTATIC_LIST_H*/

