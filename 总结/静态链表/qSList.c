#include "qSList.h"

uint8_t qSListInit(QSList* list, QSListNode* nodeBuf,uint16_t nodeNum)
{
    uint8_t ret = 0;
    uint16_t i;
    if(list!=NULL && nodeBuf!=NULL && nodeNum!=0)
    {
        ret = 1;
        list->head = NULL;
        list->free = nodeBuf;
        for(i=0; i<nodeNum-1; i++)
        {
            nodeBuf[i].next = &nodeBuf[i+1];
            nodeBuf[i].prev = NULL;
            nodeBuf[i].own = list;
        }
        nodeBuf[nodeNum-1].next = NULL;
        nodeBuf[nodeNum-1].prev = NULL;
        list->length = 0;
        list->capacity = nodeNum;
    }
    return ret;
}

void QSListFree(QSList* list, QSListNode* node)
{
    if(list!=NULL && node!=NULL)
    {
        if(node->prev != NULL)
        {
            node->next = list->free;
            list->free = node;
            node->prev = NULL;
        }
    }
}

QSListNode* QSListMalloc(QSList* list)
{
    QSListNode* ret = NULL;
    if(list!=NULL)
    {
        if(list->free != NULL)
        {
            ret = list->free;
            list->free = list->free->next;
        }
    }
    return ret;
}

void qSListDelete(QSListNode* node)
{
    QSList* list  = NULL;
    if( node!=NULL)
    {
        list = node->own;
        if(node->prev!=NULL && (list->length>0))
        {    
            if(node == list->head) /*删除头结点*/
            {
                list->head = node->next;
                list->head->prev = node->prev;
                node->prev->next = list->head;
            }
            else
            {
                node->prev->next = node->next;
                node->next->prev =  node->prev;
            }
            QSListFree(list, node);
            list->length--;
        }
    }
}

QSListNode* qSListInsertPos(QSList* list, QSListItem* item, uint16_t pos)
{
    QSListNode* node = NULL;
    if(list!=NULL && item!=NULL)
    {
        if(list->length < list->capacity)
        {
            uint16_t i;
            QSListNode* temp = NULL;
            node = QSListMalloc(list);
            node->weight = item->weight;
            node->nodeData = item->nodeData;
            if(list->length==0) /*插入第一个元素*/
            {
                list->head = node;
                list->head->next = node;
                list->head->prev = node;
            }
            else
            {
                pos = pos%(list->length+1);
                if(pos==0) /*插入在头结点位置*/
                {
                    node->prev = list->head->prev;
                    list->head->prev->next = node;
                    node->next = list->head;
                    list->head->prev = node;
                    list->head = node;
                }
                else
                {
                    temp = list->head;
                    for(i=1; i<pos; i++) /*找到对应位置的结点*/
                    {
                        temp = temp->next;
                    }
                    node->next = temp->next;
                    temp->next->prev = node;
                    
                    node->prev = temp;
                    temp->next = node;
                }
            }
            list->length++;
        }
    }
    return node;
}
/*按从小到大依次插入数据*/
QSListNode* qSListInsertMax(QSList* list, QSListItem* item)
{
    QSListNode* node = NULL;
    if(list!=NULL && item!=NULL)
    {
        if(list->length < list->capacity)
        {
            QSListNode* temp = NULL;
            node = QSListMalloc(list);
            node->weight = item->weight;
            node->nodeData = item->nodeData;
            if(list->length==0) /*插入第一个元素*/
            {
                list->head = node;
                list->head->next = node;
                list->head->prev = node;
            }
            else
            {                
                if(item->weight<=list->head->weight) /*插入在头结点位置*/
                {
                    node->prev = list->head->prev;
                    list->head->prev->next = node;
                    node->next = list->head;
                    list->head->prev = node;
                    list->head = node;
                }
                else
                {
                    temp = list->head->prev;
                    while(temp->weight>item->weight) /*找到对应位置的结点*/
                    {
                        temp = temp->prev;
                    }
                    node->next = temp->next;
                    temp->next->prev = node;
                    
                    node->prev = temp;
                    temp->next = node;
                }
            }
            list->length++;
        }
    }
    return node;
}

/**
*   函 数 名: qSListGet
*   功能说明: 获取链表中指定位置的数据
*   形    参: list：链表，pos位置
*   返 回 值: NULL：失败（list链表为空或list==NULL）
**/
QSListNode* qSListGet(QSList* list, uint16_t pos)
{
    QSListNode* ret = NULL;
    int i;
    if(list!=NULL)
    {
        if(list->length > 0)
        {
            pos = pos%list->length;
            ret = list->head;
            for(i=0; i<pos; i++) /*找到对应位置的结点*/
            {
                ret = ret->next;
            }
        }
    }
    return ret;
}
/*获取顶端（第0个位置的结点）*/
QSListNode* qSListGetTop(QSList* list)
{
    QSListNode* ret = NULL;
    if(list!=NULL)
    {
        if(list->length > 0)
        {
            ret = list->head;
        }
    }
    return ret;
}
/*获取尾部（最后一个位置的数据）*/
QSListNode* qSListGetEnd(QSList* list)
{
    QSListNode* ret = NULL;
    if(list!=NULL)
    {
        if(list->length > 0)
        {
            ret = list->head->prev;
        }
    }
    return ret;
}


/*弹出顶端（第0个位置的结点）*/
uint8_t qSListPop(QSList* list, uint16_t pos, QSListItem* item)
{
    uint8_t ret = 0;
    QSListNode* node = NULL;
    node = qSListGet(list, pos);
    if(node != NULL)
    {
        item->nodeData = node->nodeData;
        item->weight = node->weight;
        qSListDelete(node);
        ret = 1;
    }
    return ret;
}

/*弹出顶端（第0个位置的结点）*/
uint8_t qSListPopTop(QSList* list, QSListItem* item)
{
    uint8_t ret = 0;
    QSListNode* node = NULL;
    node = qSListGetTop(list);
    if(node != NULL)
    {
        item->nodeData = node->nodeData;
        item->weight = node->weight;
        qSListDelete(node);
        ret = 1;
    }
    return ret;
}

/*弹出尾部（最后一个位置的数据）*/
uint8_t qSListPopEnd(QSList* list, QSListItem* item)
{
    uint8_t ret = 0;
    QSListNode* node = NULL;
    node = qSListGetEnd(list);
    if(node != NULL)
    {
        item->nodeData = node->nodeData;
        item->weight = node->weight;
        qSListDelete(node);
        ret = 1;
    }
    return ret;
}

void qSListClear(QSList* list)
{
    if(list != NULL)
    {
        uint16_t i;
        for(i=0; list->length>0; i++)
        {
            qSListDelete(list->head);
        }
    }
}

uint16_t qSListLength(QSList* list)
{
    uint16_t length = 0;
    if(list != NULL)
    {
        length = list->length;
    }
    return length;
}

uint16_t qSListCapacity(QSList* list)
{
    uint16_t capacity = 0;
    if(list != NULL)
    {
        capacity = list->capacity;
    }
    return capacity;
}

