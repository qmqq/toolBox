#ifndef __QLIST_H_
#define __QLIST_H_

typedef struct _qListNode
{
    int    nodeVal;  /*结点的重量，用于大小排序*/
    struct _qListNode  *next; /*链表的下一个结点*/
    struct _qListNode  *prev; /*链表的前一个结点*/
    int    Container;/*Container(容器)*/
    void*  own; /*结点隶属的链表*/
}QListNode;

typedef struct
{
    uint16_t   length;   /*数据链表长度*/
    QListNode head;     /*数据链表表头*/
}QList;

uint8_t qListInit(QSList* list);
void qListDelete(QSListNode* node);
void qListInsertPos(QSList* list, QSListNode* node, int pos);
void qListInsertMax(QSList* list, QSListNode* node);
QListNode* qListGet(QList* list, uint16_t pos);
QListNode* qListGetTop(QList* list);
QListNode* qListGetEnd(QList* list);
QListNode* qListPop(QList* list, uint16_t pos);
QListNode* qListPopTop(QList* list);
QListNode* qListPopEnd(QList* list);
void qListClear(QSList* list);
uint16_t qListLength(QSList* list);

#endif /*__QSLIST_H_*/
