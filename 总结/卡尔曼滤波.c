double KalmanFilter(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
        double R = MeasureNoise_R;
        double Q = ProcessNiose_Q; /**/

        static double x_last; /*上一次的最优估测值*/
        static double p_last; /*上一次最优估测值的协方差*/
        
        double x_mid = x_last;
        double x_now;



        double p_mid ;
        double p_now;
        double kg;        

        x_mid=x_last; //当前的猜测值，x_last=x(k-1|k-1),x_mid=x(k|k-1)
        p_mid=p_last+Q; //当前估测值的协方差，p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=噪声
        kg=p_mid/(p_mid+R); //当前估测值与传感器获取的值的权值，kg为kalman filter，R为噪声
        x_now=x_mid+kg*(ResrcData-x_mid);//使用当前估计值与当前传感器获取的数据进行加权计算，估计出的最优值
                
        p_now=(1-kg)*p_mid;//最优值对应加协方差

        p_last = p_now; //更新协方差
        x_last = x_now; //更新系统状态值

        return x_now; /*返回当前的最优值*/            
}

float kalmanFilter_A(float inData) 
{
  static float prevData=0; 
  static float p=10, q=0.0001, r=0.005, kGain=0;
    p = p+q; 
    kGain = p/(p+r);

    inData = prevData+(kGain*(inData-prevData)); 
    p = (1-kGain)*p;

    prevData = inData;

    return inData; 
}

*-------------------------------------------------------------------------------------------------------------*/
/*       
        Q:过程噪声，Q增大，动态响应变快，收敛稳定性变坏
        R:测量噪声，R增大，动态响应变慢，收敛稳定性变好       
*/

#define KALMAN_Q 0.02

#define KALMAN_R 7.0000

/* 卡尔曼滤波处理 */

static double KalmanFilter(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{

    double R = MeasureNoise_R;
    double Q = ProcessNiose_Q;

    static double x_last;
    double x_mid = x_last;
    double x_now;

    static double p_last;
    double p_mid ;
    double p_now;

    double kg;

    x_mid=x_last;                       //x_last=x(k-1|k-1),x_mid=x(k|k-1)
    p_mid=p_last+Q;                     //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=噪声

    /*
     *  卡尔曼滤波的五个重要公式
     */
    kg=p_mid/(p_mid+R);                 //kg为kalman filter，R 为噪声
    x_now=x_mid+kg*(ResrcData-x_mid);   //估计出的最优值
    p_now=(1-kg)*p_mid;                 //最优值对应的covariance
    p_last = p_now;                     //更新covariance 值
    x_last = x_now;                     //更新系统状态值
    return x_now;
}