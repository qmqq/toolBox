可以定义通道映射数组，将外部设备的编号和内部接口的编号分离开。
加入有内部同类型接口共10个通道为0~9
uint8_t ch_map[10]; /*设备接口重映射表，ch_map[0~9]代表外部设备的编号，通过修改ch_map[0~9]的值可以将外部设备连接在不同的接口上*/