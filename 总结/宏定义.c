1. 使用do{}while(0)可以防止宏定义包含多个语句在if、for、while等语句中出错的可能
#define xxDEF(x)    do{ x++; x-- }while(0);

例:
#define xxDEF(x)    x++; x--

if(1)
    xxDEF(x);
本意
if(1)
{
    x++; x--;
}
实际
if(1)
    x++;
x--;
for、while与if类似

2. 宏定义参数类型检查
int *p=NULL;
#define NEED_INT(x) do{(void)(&x==p); ...}while(0)
如果x不是int型编译器将报错，指针是强类型检查

