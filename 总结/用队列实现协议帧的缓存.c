typedef struct
{
    uint8_t Buff[MAX_FRAME_BUFFER_NUM][MAX_FRAME_BUFFER_LENGTH];
    uint8_t frameCount;
    uint8_t rdPoint;
    uint8_t wrPoint;
}Frametypedef;

uint8_t inFrameQueue(Frametypedef* frameBuff,char *buff, uint16_t length)
{
	if(frameCount == MAX_FRAME_BUFFER_NUM)
	{
		return 0; //插入失败
	}
	wrPoint = wrPoint % MAX_FRAME_BUFFER_NUM;
	memcpy(frameBuff->Buff[wrPoint++],buff,length);
	frameCount++;
	return 1;
}

uint8_t* outFrameQueue(Frametypedef* frameBuff)
{
	uint8_t* ret = NULL;
	if(frameCount == 0)
	{
		return NULL; //队列里没有数据
	}
	rdPoint = rdPoint %MAX_FRAME_BUFFER_NUM;
	ret = (uint8_t*)frameBuff->Buff[rdPoint++];
	frameCount--;
	return ret;
}