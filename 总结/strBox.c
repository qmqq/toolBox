#include "stdio.h"
#include "string.h"

/*
字符串转整数
注意：当遇到非数字字符（不包括符号字符）时会停止转换，返回非数字字符之前转换结果
例如：
"-123s56.12" 转换后=-123
*/
int str2Int(const char *str)
{
    int ret = 0;
    sscanf(str,"%d",&ret);
    return ret;
}

/*
字符串转浮点数
注意：当遇到非浮点数字符时会停止转换，返回非浮点数字符之前的转换结果
例如：
"-12.56s.3" 转换后=-12.56
*/
float str2Float(const char *str)
{
    float ret = 0.0;
    sscanf(str,"%f",&ret);
    return ret;
}

/*
字符串转16进制数
注意：当遇到非16进制字符时会停止转换，返回当前已经转换的16进制数的个数
      hexArry的空间大小应该是str字符串长度的一半，即strlen(str)=3,则hexArry大小=2；
例如：
"AABBCCSSDDWW" 转换后=0xAA 0xBB 0xCC，函数返回3
*/
uint16_t str2Hex(const char* str, char* const hexArry)
{
    uint16_t i=0,j=0;
    uint16_t length = strlen(str);
    char buffer[3] = {0};
    
    if(length%2 != 0)
    {
        i=1;
        buffer[0] = '0';
        buffer[1] = str[0];
        buffer[2] = '\0';
        sscanf(buffer,"%X",&hexArry[j++]);
    }
    
    for(i; i<length; i+=2)
    {
        buffer[0] = str[i];
        buffer[1] = str[i+1];
        buffer[2] = '\0';
        if(sscanf(buffer,"%X",&hexArry[j++]) == 0)
        {
        	j--;
			break;
		}
    }
    return j;
}

/*
16进制数转字符串
注意：strBuf的空间大小应该是hexArry长度的一倍+1（存储'\0'）；
例如：
0xAA 0xBB 0xCC 转换后="AABBCC" ，函数返回6
*/
uint16_t hex2Str(const char* hexArry, char* const strBuf, uint16_t sourceLen)
{
    uint16_t i,j=0;
    for(i=0; i<sourceLen; i++)
    {
        sprintf(&strBuf[j],"%X",hexArry[i]&0xFF);
        j+=2;
    }
    return j;
}

/*
去除字符串中所有的指定的字符串
返回最后一个删除的位置
*/
char* c_deleteAll(char* const str, const char* deleteStr)
{
    char* p = NULL;
    uint16_t i,len=strlen(deleteStr);
    if(str == NULL || deleteStr == NULL)
    {
        return p;
    }
    do 
    {
        p = strstr(str, deleteStr);
        if(p != NULL)
        {
            for(i=0; p[i+len]; i++)
            {
                p[i] = *(p+i+len);
            } 
            p[i] = '\0';
        }
        else
        {
            break;
        }
    }while(1);
    return p;
}

/*
去除字符串中第一次出现的指定字符串
返回删除所在的位置
*/
char* c_delete(char* const str, const char* deleteStr)
{
    char* p = NULL;
    uint16_t i,len=strlen(deleteStr);
    if(str == NULL || deleteStr == NULL)
    {
        return p;
    }
    p = strstr(str, deleteStr);
    if(p != NULL)
    {
        for(i=0; p[i+len]; i++)
        {
            p[i] = *(p+i+len);
        } 
        p[i] = '\0';
    }
    return p;
}

uint16_t c_split(char* str, const char *delim, char* list[], uint16_t listLen)
{
    uint16_t i=0;
    if(str==NULL || delim==NULL || list==NULL || listLen==0)
    {
        return i;
    }
    list[i++] = strtok(str, delim);
    for(i; i<listLen; i++)
    {
        list[i] = strtok(NULL, delim);
        if(list[i] == NULL)
        {
            break;
        }
    }
    return i;
}

/*
删除字符串中指定位置的字符串
*/
void c_erase(char* const str, uint16_t start, uint16_t num)
{
    char* p = str;
    uint16_t i,len = strlen(str);
    if(str == NULL || len<=start)
    {
        return;
    }
    if(start+num < len)
    {
        for(i=start; p[i+num]; i++)
        {
            p[i] = *(p+i+num);
        } 
        p[i] = '\0';
    }
    else
    {
		p[start] = '\0';
	}
}

/*
从字符串中取出指定位置和数量的字符
*/
uint16_t c_mid(char* const str, char* const recStr,uint16_t start, uint16_t num)
{
    uint16_t i,j,len = strlen(str);
    if(str == NULL || len<=start)
    {
        return;
    }
    for(i=start; str[i] && num>0; i++, num--)
    {
        recStr[j++] = str[i];
    } 
    recStr[j] = '\0';
    return j;
}

/*
查找两个字符串之间的字符串
返回结束字符串在原始字符串中所在的位置
*/
char* c_smid(char* const str, char* const recStr,const char* start, const char* end)
{
    char* p_start=NULL;
	char* p_end=NULL;
    uint16_t i=0;
    if(str==NULL || start==NULL || end==NULL)
    {
        return;
    }
    p_start = strstr(str, start);
    p_start += strlen(start);
    p_end   = strstr(p_start, end);
    if((start != NULL) && (end != NULL))
    {
        
        for(i=0; (p_start+i)<p_end; i++)
        {
            recStr[i] = p_start[i];
        }
        recStr[i] = '\0';
    }
    return p_end;
}

/*
去除字符串中所有的空格
*/
void c_trim(char* const str)
{
    char* p = str;
    uint16_t i=0;
    if(str == NULL)
    {
        return;
    }
    for(i=0; str[i]; i++)
    {
        if(str[i] != ' ')
        {
            *p++ = str[i];
        }
    }
    *p = '\0';
}

/*
去除字符串左边的空格
*/
void c_ltrim(char* const str)
{
    char* p = str;
    uint16_t i=0;
    if(str == NULL ||  *str != ' ')
    {
        return;
    }
    
    for(i=1; str[i];i++)
    {
        if(str[i] != ' ')
        {
            break;
        }
    }
    for(i; str[i];i++)
    {
        *p++ = str[i];
    }
    *p = '\0';
}

/*
去除字符串右边的空格
*/
void c_rtrim(char* const str)
{
    char* p = str;
    uint16_t i=strlen(str)-1;
    if(str == NULL)
    {
        return;
    }
    
    for(i; i>0;i--)
    {
        if(str[i] != ' ')
        {
            break;
        }
    }
    p[i+1] = '\0';
}

/*
去除字符串两边的空格
*/
void c_rltrim(char* const str)
{
    c_ltrim(str);
    c_rtrim(str);
}

/*
linux平台下的strtok函数的线程安全版
用法请参考百度
*/
/* Parse S into tokens separated by characters in DELIM. 
   If S is NULL, the saved pointer in SAVE_PTR is used as 
   the next starting point.  For example: 
        char s[] = "-abc-=-def"; 
        char *sp; 
        x = strtok_r(s, "-", &sp);      // x = "abc", sp = "=-def" 
        x = strtok_r(NULL, "-=", &sp);  // x = "def", sp = NULL 
        x = strtok_r(NULL, "=", &sp);   // x = NULL 
                // s = "abc\0-def\0" 
*/  
char *strtok_r(char *s, const char *delim, char **save_ptr) {  
    char *token;  
  
    if (s == NULL) s = *save_ptr;  
  
    /* Scan leading delimiters.  */  
    s += strspn(s, delim);  
    if (*s == '\0')   
        return NULL;  
  
    /* Find the end of the token.  */  
    token = s;  
    s = strpbrk(token, delim);  
    if (s == NULL)  
        /* This token finishes the string.  */  
        *save_ptr = strchr(token, '\0');  
    else {  
        /* Terminate the token and make *SAVE_PTR point past it.  */  
        *s = '\0';  
        *save_ptr = s + 1;  
    }  
  
    return token;  
}  

/*
将一个字符串切割成一组字符串列表，不包含空串
*/
uint16_t c_split(char* str, const char *delim, char* list[], uint16_t listLen)
{
    uint16_t i=0;
    char* save_ptr = NULL;
    if(str==NULL || delim==NULL || list==NULL || listLen==0)
    {
        return i;
    }
    list[i++] = strtok_r(str, delim, &save_ptr);
    for(i; i<listLen; i++)
    {
        list[i] = strtok_r(NULL, delim, &save_ptr);
        if(list[i] == NULL)
        {
            break;
        }
    }
    return i;
}

/*
将一个字符串切割成一组字符串列表，包含空串

*/
uint16_t c_split_s(char* str, const char *delim, char* list[], uint16_t listLen)
{
	uint16_t i=0;
	char* p = NULL;
	if(str==NULL || delim==NULL || list==NULL || listLen==0)
    {
        return i;
    }
    list[i++] = str;
	p = strpbrk(str, delim);
	if(p == NULL)
	{
		return i;
	}
	list[i++] = p+1;
	*p = '\0';
	p++;
    do
    {
    	p = strpbrk(p, delim);
    	list[i++] = p+1;
    	if(p != NULL)
    	{
			*p = '\0';
		}
    	else
    	{
    		break;
		}
		p++;
    }while(i<listLen);
    return i-1;
}