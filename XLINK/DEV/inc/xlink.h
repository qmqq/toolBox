#ifndef __XLINK_H_
#define __XLINK_H_

#define XLINK_HEAD_1	(0xAA)
#define XLINK_HEAD_2	(0x55)
#define XLINK_SYS		(0x01) 

#define XLINK_CH_NUM	(5) /*通道数*/
#define XLINK_DATA_SIZE	(10) /*数据区的大小*/

#include "string.h"
#include <stdio.h>
#include "typedef.h"

typedef enum
{
	XLINK_STATU_NOT_INIT,
	XLINK_STATU_GO_HEAD1,
	XLINK_STATU_GO_HEAD2,
	XLINK_STATU_GO_LEN,
	XLINK_STATU_GO_DATA
}xlink_stu_m;

struct _xlink_statu_s
{
	uint32_t 	recCycle; /*帧与帧之间接收的周期单位ms*/ 
	uint32_t 	recFrameNum; /*已经接收的帧数目*/ 
	xlink_stu_m parse_state; /*接收状态*/
	uint8_t 	recNum; /*已经接收的字符个数*/
	uint8_t 	len; /*有效数据的长度*/
	uint8_t  	lost; /*记录每256帧丢失的帧数*/
}; 
typedef struct _xlink_statu_s XlinkStu_t;


struct _xlink_s
{
	uint8_t head[2]; /*帧头*/
	uint8_t len; /*帧长度（指的是从帧都开始至校验位之前的数据长度）*/ 
#ifdef XLINK_SYS
	uint8_t sys; /*系统编号（用于区分不同的系统）*/ 
#endif
	uint8_t id;  /*帧ID*/ 
	uint8_t index; /*帧编号*/ 
	uint8_t data[XLINK_DATA_SIZE];
	uint8_t check;
};
typedef struct _xlink_s XlinkFrame_t;

struct _handle_frame_s
{
	uint8_t id;  /*帧ID*/ 
	int (*handleFrameFun) (XlinkFrame_t *frame);
};
typedef struct _handle_frame_s xlinkHandleFrame_t;

uint8_t xlinkParse(uint8_t chan, uint8_t _byte, XlinkFrame_t* recFrame, XlinkStu_t* recStatus);
uint8_t xlinkFrameSend( uint8_t chan, uint8_t id, void* data, uint8_t len ); 
void xlinkPrintf(XlinkFrame_t* frame);
#endif /*__XLINK_H_*/
