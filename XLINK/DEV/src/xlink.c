#include "xlink.h"


/*定义xlink状态结构体*/ 
XlinkStu_t   xlinkStatus[XLINK_CH_NUM] = {0};
XlinkFrame_t xlinkRxFrame[XLINK_CH_NUM] = {0};
XlinkFrame_t xlinkTxFrame[XLINK_CH_NUM] = {0};

/*xlink发送函数（移植需注意）*/
uint8_t xlinkSend(uint8_t ch ,uint8_t *sendBuf, uint8_t len)
{
	uint8_t i;
	for(i = 0; i<len; i++)
	{
		printf("%02X ", sendBuf[i]);
	}
	return 0;
}

/*xlink接收函数（移植需注意）,返回0失败，返回1 成功*/ 
uint8_t xlinkGet(uint8_t channal, uint8_t *rec)
{
	scanf("%X",rec);
	return 1;
}


#define FRAME_CHECK(buff, len)  checkSum(buff, len)
#define XLINK_GET_US_TIME() 	(1)

/*和校验*/
uint8_t checkSum(uint8_t* buff, uint8_t len)
{
	uint8_t ret=0, i;
	for(i = 0; i< len; i++)
	{
		ret += buff[i];
	}
	return ret;
}

/*帧协议解析*/
uint8_t xlinkParse(uint8_t chan, uint8_t _byte, XlinkFrame_t* recFrame, XlinkStu_t* recStatus)
{
	static count[XLINK_CH_NUM] = {0}; /*用于顺序存储帧数据*/ 
	static frameCount[XLINK_CH_NUM] = {0}; /*用于计算丢帧率*/
	static uint64_t cycleTime[XLINK_CH_NUM] = {0}; /*用于计算帧接收的周期*/ 
	uint8_t* framePoint =  (uint8_t*)&xlinkRxFrame[chan];
	uint8_t ret = 0;
	recFrame = NULL;
	recStatus = NULL;
	if(xlinkStatus[chan].parse_state == XLINK_STATU_NOT_INIT)
	{
		return ret;
	}
	xlinkStatus[chan].recNum++;
	switch(xlinkStatus[chan].parse_state)
	{
		/*帧头*/
		case XLINK_STATU_GO_HEAD1:
			count[chan] = 0;
			if(_byte == XLINK_HEAD_1)
			{
				framePoint[count[chan]++] = _byte;
				xlinkStatus[chan].parse_state = XLINK_STATU_GO_HEAD2;
			}
			break;
		case XLINK_STATU_GO_HEAD2:
			if(_byte == XLINK_HEAD_2)
			{
				framePoint[count[chan]++] = _byte;
				xlinkStatus[chan].parse_state = XLINK_STATU_GO_LEN;
			}
			else
			{
				xlinkStatus[chan].parse_state = XLINK_STATU_GO_HEAD1;
			}
			break;		
		case XLINK_STATU_GO_LEN:
			framePoint[count[chan]++] = _byte;
			xlinkStatus[chan].parse_state = XLINK_STATU_GO_DATA;
			break;				
		case XLINK_STATU_GO_DATA:
			framePoint[count[chan]++] = _byte;
			if(count[chan] > xlinkRxFrame[chan].len) /*获取到完整帧*/
			{
				if( xlinkRxFrame[chan].check == FRAME_CHECK(framePoint, xlinkRxFrame[chan].len) ) /*解析到正确帧*/
				{
					uint64_t temp;
					frameCount[chan]++;
					/*计算丢帧率*/ 
					if(xlinkRxFrame[chan].index == 0xFF)
					{
						xlinkStatus[chan].lost = 0xFF - frameCount[chan]; 
						frameCount[chan] = 0;
					}
					
					/*计算接收周期*/ 
					temp = XLINK_GET_US_TIME();
					if(temp>cycleTime[chan])
					{
						xlinkStatus[chan].recCycle = temp - cycleTime[chan];
					}
					cycleTime[chan] = temp;
					recFrame = &xlinkRxFrame[chan];
					recStatus = &xlinkStatus[chan];
					ret = 1;
				}
				xlinkStatus[chan].parse_state = XLINK_STATU_GO_HEAD1;
			}			
	}
	return ret;
}

/*帧协议发送*/
uint8_t xlinkFrameSend(uint8_t chan, uint8_t id, void* data, uint8_t len )
{
	if(len>XLINK_DATA_SIZE)
	{
		return 0;
	}
	xlinkTxFrame[chan].head[0] = XLINK_HEAD_1;
	xlinkTxFrame[chan].head[1] = XLINK_HEAD_2;
#ifdef XLINK_SYS
	xlinkTxFrame[chan].sys = XLINK_SYS;
#endif
	xlinkTxFrame[chan].id = id;
	memcpy(xlinkTxFrame[chan].data, data, len);
	xlinkTxFrame[chan].len = sizeof(XlinkFrame_t)-sizeof(xlinkTxFrame[chan].check);
	xlinkTxFrame[chan].check = FRAME_CHECK((uint8_t*)&xlinkTxFrame[chan], xlinkTxFrame[chan].len);
	xlinkSend(chan ,(uint8_t*)&xlinkTxFrame[chan], sizeof(XlinkFrame_t));
	xlinkTxFrame[chan].index++;
	return 1;
}

/*打印帧内容*/
void xlinkPrintf(XlinkFrame_t* frame) 
{
	uint16_t i,len = sizeof(XlinkFrame_t);
	uint8_t *p = (uint8_t*)frame;
	for(i = 0; i<len; i++)
	{
		printf("%02X ", p[i]);
	}
	printf("\r\n"); 
}
