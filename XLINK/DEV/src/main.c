#include <stdio.h>
#include <stdlib.h>
#include "typedef.h"

#define mySend(data) xlinkFrameSend(0, 0xAA, (uint8_t*)&data, sizeof(data) ); 
typedef struct
{
	uint8_t a;
	uint8_t b;
	uint8_t c;
	uint8_t d;
	uint8_t e;
	uint8_t f;
	uint8_t g;
}TestFrame;
TestFrame testFrame = {0x01,0x02,0x03,0x04,0x05,0x06,0x07};
int main(int argc, char *argv[]) {
	mySend(testFrame);
	printf("\r\n");
	mySend(testFrame);
	printf("\r\n");
	mySend(testFrame);
	xlinkRxCheck();
	return 0;
}
