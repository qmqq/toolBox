#include "xlink.h"

/*接收处理函数*/ 
int myHandleFun(XlinkFrame_t *frame)
{
	xlinkPrintf(frame);
	return 0;
}
/*接收处理结构体*/ 
xlinkHandleFrame_t myHandle = 
{
	.id = 0xAA, /*对应的ID号*/ 
	.handleFrameFun = myHandleFun /*对应的处理函数*/ 
};


/*所有的接收处理任务存在这个表中*/
xlinkHandleFrame_t* handleFrameList[] = 
{
	&myHandle,
	NULL /*用于表明数组的结束位，不能删除*/ 
};

void xlinkRxCheck( void )
{
	char rcv;
	uint8_t i;
	XlinkFrame_t* recFrame= NULL; 
	XlinkStu_t* recStatus;
	while(1)
	{
		if(xlinkGet(0, &rcv) != 0)
		{
			if(xlinkParse(0, rcv, recFrame, recStatus) != 0)
			{
				printf("rcv ok\r\n");
				#ifdef XLINK_SYS
				if(recFrame->sys != XLINK_SYS)
				{
					//continue;
					return;
				}
				#endif
				for(i=0; handleFrameList[i]; i++)
				{
					if(handleFrameList[i]->id == recFrame->id)
					{
						handleFrameList[i]->handleFrameFun(recFrame);
					}
				}
			}
		}
	}
	
}


