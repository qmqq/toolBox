#ifndef __DEBUG_LOG_H_
#define __DEBUG_LOG_H_
#include "stdio.h"

	#if !defined LOG_LEVEL_H && !defined LOG_LEVEL_M && !defined LOG_LEVEL_L && !defined LOG_LEVEL_NONE
		/*****************************************************************/ 
		#define LOG_LEVEL_L //设置默认显示日志的级别 
		/*****************************************************************/ 
	#endif
	
	#ifdef LOG_FULL_DIS //显示详细信息 
		#define _LOG(format,arg...)	 printf("FUN:%-10s " "LINE:%-4d ==>"format, __FUNCTION__  ,__LINE__, ##arg)
	#else  //只显示调试输出的数据 
		#define _LOG(format,arg...)	 printf(format, ##arg)
	#endif
	
	#ifdef LOG_LEVEL_H //只显示最高级别的日志 
		#define LOG_H(format,arg...)	_LOG(format, ##arg)
		#define LOG_M(format,arg...)	((void)0) 
		#define LOG_L(format,arg...)	((void)0) 
	#endif
	
	#ifdef LOG_LEVEL_M //显示最高和中等优先级日志 
		#define LOG_H(format,arg...)	_LOG(format, ##arg)
		#defi ne LOG_M(format,arg...)	_LOG(format, ##arg)
		#define LOG_L(format,arg...)	((void)0) 
	#endif 
	
	#ifdef LOG_LEVEL_L //全部显示
		#define LOG_H(format,arg...)	_LOG(format, ##arg)
		#define LOG_M(format,arg...)	_LOG(format, ##arg)
		#define LOG_L(format,arg...)	_LOG(format, ##arg)
	#endif
	
	#ifdef LOG_LEVEL_NONE //都不显示
		#define LOG_H(format,arg...)	((void)0) 
		#define LOG_M(format,arg...)	((void)0) 
		#define LOG_L(format,arg...)	((void)0)
	#endif

#define LOG(format,arg...)	LOG_L(format,##arg)	

#endif /*__DEBUG_LOG_H_*/

