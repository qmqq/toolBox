#define LOG_FULL_DIS // 设置显示模式为显示详细信息 
#define LOG_LEVEL_H //设置显示等级为只显示最高级信息 

#include <stdio.h>
#include <stdlib.h>
#include "DebugLog.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

__attribute__((constructor)) void beforMain(void)
{
	printf("befor main\r\n");
}

__attribute__((destructor)) void afterExti(void)
{
	printf("after main\r\n");
}

int main(int argc, char *argv[]) {
	LOG("nihao:%s\r\n","LOW");
	LOG_M("nihao:%s\r\n","MID");
	LOG_H("nihao:%s\r\n","HIGH");
	return 0;
}
