#ifndef __LIST_H_
#define __LIST_H_

typedef unsigned char 	uint8_t;

typedef unsigned short 	BaseType;
typedef unsigned int    TickType;

#define LIST_MAX_LENGTH         255

typedef struct _Tga_Item
{
    TickType            itemVal;
    BaseType            pxNext;
    BaseType            pxPre;
    char				empty; //-1������ 
    void *              value;
}ItemType;

typedef struct
{
	TickType            itemVal;
	void *              value;
}MiniItemType;

typedef struct
{
	BaseType capacity;
	BaseType length;
	ItemType *heade;
	BaseType  end; 
}ListType;

uint8_t ListInit(ListType* list, ItemType* buffer, BaseType bufLen);
uint8_t ListInsert(ListType* list, MiniItemType* Item);
BaseType listCapacty(ListType* list);
BaseType listength(ListType* list);
uint8_t listGet(ListType* list, MiniItemType* item, BaseType pos);
uint8_t listPop(ListType* list, MiniItemType* item);
uint8_t listDelete(ListType* list, MiniItemType* item, BaseType pos); 
#endif /*__LIST_H_*/
