#include "STimer.h"
#include "malloc.h"
#include "stdio.h"

#define MAX_DELAY			0xffffffff
#define __DISABLE_INTER() 	((void)0);
#define __ENABLE_INTER() 	((void)0);

/*定时器操作*/
StimerIdType softTimerCreate(TickType period, timerFun callBack, uint8_t opt);
uint8_t softTimerStart(StimerIdType timerID);
uint8_t softTimerStop(StimerIdType timerID);
uint8_t softTimerDelete(StimerIdType *timerID); 

/*定时器状态查询*/ 
uint8_t softTimerState(StimerIdType timerID);
uint8_t softTimerPeriod(StimerIdType timerID);
uint8_t softTimerOpt(StimerIdType timerID);
TickType softTimerTick(void);

/*定时器设置*/ 
uint8_t softTimerSetOpt(StimerIdType timerID,uint8_t opt);
uint8_t softTimerSetCallBack(StimerIdType timerID,timerFun callBack);
uint8_t softTimerSetPeriod(StimerIdType timerID,TickType period);

/*用来注册所有定时器函数*/ 
__softTimerAllFun STimer= 
			{softTimerCreate,
			softTimerStart,
			softTimerStop,
			softTimerDelete,
			softTimerState,
			softTimerPeriod,
			softTimerOpt,
			softTimerTick,
			softTimerSetPeriod,
			softTimerSetCallBack,
			softTimerSetOpt
			};

/*定时器计数器和最近一次到时时间*/ 
static TickType	timerTick = 0;
static TickType recentTime 	= 0;

/*定义两个静态的列表项数组*/
static ItemType timerItem1[SOFT_TIMER_MAX_MUN+1];
static ItemType timerItem2[SOFT_TIMER_MAX_MUN+1];

/*定义两个链表*/ 
static ListType List1;
static ListType List2;

/*定义currrent指针指向当前在使用的链表，和overFlow指针指向溢出的链表*/ 
static ListType *currentTList = NULL;
static ListType *overflowTList = NULL;

/*用来存储已经创建的定时器的个数*/ 
static BaseType timerCount = 0;

/*初始化操作，将链表与链表空间链接起来*/ 
static void softTimerInit(void)
{
	ListInit(&List1,timerItem1,SOFT_TIMER_MAX_MUN+1);
	ListInit(&List2,timerItem2,SOFT_TIMER_MAX_MUN+1);
	currentTList = &List1;
	overflowTList = &List2;
}

/*
 * 使已经创建的定时器/到时的定时器/停止的定时器运行起来 
 * timerID：		定时器ID
 * 返回： 0：失败	1：成功 
 */ 
static uint8_t softTimerRun(StimerIdType timerID)
{

	MiniItemType item;
	TickType time;
	SftTimerType *timer = (SftTimerType*)timerID;

	if(timer != NULL)
	{
		if( (timer->state != TIMER_STATE_RUNING) &&
			(timer->state != TIMER_STATE_DELETE)
		  ) //不是运行态，一定不在current和overFlow列表中
		{
			item.value = timer;
			time = timerTick; //获取当前时间
			item.itemVal =  time + timer->period;
			__DISABLE_INTER();
			if(time > item.itemVal)   //溢出了
			{
				ListInsert(overflowTList, &item);
			}
			else
			{
				ListInsert(currentTList, &item);
			}
			__ENABLE_INTER()
			timer->state = TIMER_STATE_RUNING;
			return 1;
		}
	}
	return 0;
}

/*
 * 用来将列表中不是运行态的定时器从列表中删除，列表中只存留运行态的定时器 
 * list		定时器列表
 * 返回： 0：失败	1：成功 
 */ 
static uint8_t timerListCheck(ListType *list)
{
	BaseType i;
	BaseType length;
	MiniItemType item;
	SftTimerType *timer;
	if(list != NULL)
	{
		length  = listLength(list);
		for(i = 0; i<length; i++)
		{
			listGet(list,&item,i);
			timer = item.value;
			if(timer->state != TIMER_STATE_RUNING)
			{
				listDelete(list,&item, i);
			}
		}
		return 1;
	}
	return 0;
}

/*
 * 创建一个定时器，返回定时器ID 
 * period：			定时周期
 * callBack： 		回调函数 
 * opt：			定时器选项  TIMER_OPT_SINGLE单次运行
 *								TIMER_OPT_REPEATE 重复运行		
 * 返回：定时器ID				 
 */ 
StimerIdType softTimerCreate(TickType period, timerFun callBack, uint8_t opt)
{
	SftTimerType *ret = NULL;
	
	if(currentTList == NULL) //如果没有初始化，就初始化一下 
	{
		softTimerInit();
	}
	
	if(timerCount < SOFT_TIMER_MAX_MUN)
	{
		ret = (SftTimerType *)malloc(sizeof(SftTimerType));
		timerCount++;
	}
	if(ret != NULL)
	{
		ret->callBack = callBack;
		ret->period   = period;
		ret->opt 	  = opt;
		ret->state 	  = TIMER_STATE_UNUSED;
	}
	return (StimerIdType)ret;
}

/*
 * 用户使用的功能函数，用来将一个创建的/停止的/到时的定时器启动起来
 * timerID 		定时器ID
 * 返回： 0：失败	1：成功 
 */ 
uint8_t softTimerStart(StimerIdType timerID)
{
	uint8_t ret = 0;
	if(timerID != NULL)
	{
		ret = softTimerRun(timerID);
	}
	return ret;
}

/*
 * 用户使用的功能函数，使一个 正在运行的定时器停止 
 * timerID 		定时器ID
 * 返回： 0：失败	1：成功 
 */ 
uint8_t softTimerStop(StimerIdType timerID)
{
	SftTimerType *timer = (SftTimerType*)timerID;
	if( (timer != NULL) && //定时器指针不为空 
		(timer->state == TIMER_STATE_RUNING) //定时器处在运行状态 
	  )
	{
		timer->state = TIMER_STATE_STOPPED;
		timerListCheck(currentTList);
		timerListCheck(overflowTList);
		return 1;
	}
	return 0;
}

/*
 * 用户使用的功能函数，删除一个已经创建的定时器 
 * timerID 		定时器ID
 * 返回： 0：失败	1：成功 
 */ 
uint8_t softTimerDelete(StimerIdType *timerID)
{
	SftTimerType *timer = (SftTimerType*)(*timerID);
	if(timer != NULL)
	{
		uint8_t temp;
		__DISABLE_INTER();
		temp =  timer->state; //保存当前定时器的状态 
		timer->state = TIMER_STATE_DELETE; //设置定时器状态为删除态 
		/*找到定时器并删除它*/ 
		timerListCheck(currentTList);
		timerListCheck(overflowTList);
		
		if(temp == TIMER_STATE_NOW) //定时器的相关数据是否已经取出并正在使用 
		{
			/*让已经取出使用的值从定时器任务中删除掉*/ 
			TickType temp;
			recentTime = timerTick;
			if(recentTime == MAX_DELAY)
			{
				recentTime = 0;
			}
			temp = recentTime;
			__ENABLE_INTER();
			/*定时器任务已经没有在使用当前定时器的任何值了*/ 
			while(temp != recentTime)
			{
				free(timer); //释放申请的内存 
				*timerID = NULL; //标记此定时器ID不可用 
			}
		}
		else //定时器任务没有在使用当前定时器 
		{
			free(timer); //释放申请的内存 
			*timerID = NULL; //标记此定时器ID不可用 
		}
		timerCount--;
		return 1;
	}
	return 0;
}

/*
 * 定时器相关数据查询指令 
 */ 
uint8_t softTimerState(StimerIdType timerID)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		ret = timer->state;
	}
	return ret;
}
uint8_t softTimerPeriod(StimerIdType timerID)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		ret = timer->period;
	}
	return ret;
}
uint8_t softTimerOpt(StimerIdType timerID)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		ret = timer->opt;
	}
	return ret;
}
/*
 *获取定时器当前tick值 
 */ 
TickType softTimerTick(void)
{
	return timerTick;
}

/*
 *定时器设置相关函数 
 */
uint8_t softTimerSetPeriod(StimerIdType timerID,TickType period)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		timer->period = period;
	}
	return ret;
}
uint8_t softTimerSetCallBack(StimerIdType timerID,timerFun callBack)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		timer->callBack = callBack;
	}
	return ret;
}
uint8_t softTimerSetOpt(StimerIdType timerID,uint8_t opt)
{
	uint8_t ret = TIMER_ERR;
	SftTimerType *timer = (SftTimerType*)timerID;
	if(timer != NULL)
	{
		timer->opt = opt;
	}
	return ret;
}


void timerTask(void)
{
	static MiniItemType timerTaskItem;
	static SftTimerType	*timerTaskTimer = NULL;
	static TickType 	timeTemp = 0;
	
	timerTick++;
	if(recentTime <= timerTick)   //定时器到时间了
	{
		NextOutTimerDead: // 此时有定时器同时到时会goto到这里 
		 
		if(timerTaskTimer != NULL)
		{
			if(timerTaskTimer->state != TIMER_STATE_DELETE) //确定当前定时器没有被删除 
			{
				if(timerTaskTimer->state != TIMER_STATE_STOPPED) //此定时器没有被停止 
				{
					timerTaskTimer = timerTaskItem.value;
					timerTaskTimer->state = TIMER_STATE_COMPLETED; //设置定时器完成标志 
					if(timerTaskTimer->callBack != NULL)
					{
						timerTaskTimer->callBack(timerTick); //运行定时器回调函数 
					}
					if(timerTaskTimer->opt == TIMER_OPT_REPEATE) //如果定时器是周期性的 
					{
						timeTemp = timerTick;
						timerTaskItem.itemVal = timeTemp + timerTaskTimer->period; //重新计算到时时间 
						
						timerTaskItem.value = timerTaskTimer;
						timerTaskTimer->state = TIMER_STATE_RUNING; //标记定时器处于运行态 
						if(timeTemp>timerTaskItem.itemVal) //溢出了
						{
							ListInsert(overflowTList, &timerTaskItem); //插入到移除列表中 
						}
						else
						{
							ListInsert(currentTList, &timerTaskItem); //插入到当前列表中 
						}
					}
				}
			}
			timerTaskTimer = NULL;
		}
		recentTime = MAX_DELAY;

		/*获取下一个定时时间*/
		if(listPop(currentTList, &timerTaskItem))
		{
			timerTaskTimer = timerTaskItem.value;
			recentTime = timerTaskItem.itemVal;
			if(recentTime <= timerTick ) //如果当前取出的定时器也到时了 
			{
				goto NextOutTimerDead;
			}
			timerTaskTimer->state = TIMER_STATE_NOW;//标记定时器任务真正使用此定时器的数据 
		}
	}
	/*列表的转换*/
	if(timerTick == 0) //定时器tick溢出之后，将当前列表指针与溢出列表指针对换，运行之前溢出的定时器 
	{
		ListType *temp;
		temp = currentTList;
		currentTList = overflowTList;
		overflowTList = temp;
	}
}
