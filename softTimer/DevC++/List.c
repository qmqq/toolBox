#include <stdio.h>
#include "List.h"

/*
 * 列表的初始化 
 * list：		要初始化的列表
 * buffer：		用来存储列表的空间
 * bufLen：		列表空间的大小（列表的大小是列表空间大小-1） 
 * 返回0：失败	1：成功 
 */
uint8_t ListInit(ListType* list, ItemType* buffer, BaseType bufLen)
{
	BaseType i;
    if(bufLen > LIST_MAX_LENGTH)
    {
        return 0;
    }
    list->heade = buffer;
    list->heade->pxNext = 0;
    list->heade->pxPre = 0;
    list->heade->itemVal = 0;
    list->end = 0;
    list->capacity = bufLen-1;
    list->length = 0;
    for(i = 0; i<bufLen; i++) //清空所有存储区 
    {
		buffer[i].value = NULL;
		buffer[i].empty = -1;
	}
    return 1;
}

/*
 * 列表的插入，会根据列表项的值从小到大排序
 * list：		要插入的列表
 * Item：		要插入的列表项 
 * 返回0：失败	1：成功 
 */ 
uint8_t ListInsert(ListType* list, MiniItemType* Item)
{
	uint8_t  ret = 0;
	BaseType i;
	BaseType current;
	BaseType forward;
	/*检查*/
	ret = (list!=NULL) && (Item!=NULL);
	if( !ret ||  (list->length >= list->capacity))
	{
		return 0;
	}
	
	/*寻找合适的插入点*/
	for(current = list->heade->pxNext; list->heade[current].pxNext != 0; current = list->heade[current].pxNext)
	{
		if(list->heade[current].itemVal >= Item->itemVal)
		{
			break;
		}
	}
	forward = list->heade[current].pxPre;
	
	/*查找一个空闲的地方*/
	for(i=1; i<list->capacity; i++)
	{
		if(list->heade[i].empty == -1)
		{
			break;
		}
	} 
	
	/*插入到最后一个*/ 
	if( (list->heade[current].pxNext==0) && 
	    (list->heade[current].itemVal < Item->itemVal))
	{
		list->heade[current].pxNext = i;
		list->end = i;
		list->heade[i].pxNext = 0;
		list->heade[i].pxPre = current;
		list->heade[i].itemVal = Item->itemVal;
		list->heade[i].value = Item->value;
		list->heade[i].empty = 1;
	} 
	else
	{
		list->heade[forward].pxNext = i;
		list->heade[i].pxPre = forward;
		
		list->heade[current].pxPre = i;
		list->heade[i].pxNext = current;

		list->heade[i].itemVal = Item->itemVal;
		list->heade[i].value = Item->value;
		list->heade[i].empty = 1;
	}
	
	list->length++;
	return 1;
}

/*
 * 获取列表中第pos项的值 
 * list：		从此列表获取
 * item：		用来存储返回的列表项
 * 返回0：失败	1：成功 
 */ 
uint8_t listGet(ListType* list,  MiniItemType* item, BaseType pos)
{
	uint8_t ret = 0;
	uint8_t i;
	BaseType current;
	if( pos>=list->capacity ) 
	{
		return 0;
	}
	if( (list != NULL) && (list->length != 0) )
	{
		current = list->heade->pxNext;
		for(i=0; i<pos; i++)
		{
			current = list->heade[current].pxNext;
		}
		item->itemVal = list->heade[current].itemVal;
		item->value = list->heade[current].value;
		ret = 1;
	}
	return ret;
}

/*
 * 删除列表中第pos项的值 
 * list：		从此列表删除 
 * item：		用来存储返回的列表项
 * 返回0：失败	1：成功 
 */ 
uint8_t listDelete(ListType* list, MiniItemType* item, BaseType pos)
{
	uint8_t ret = 0;
	uint8_t i;
	BaseType current;
	BaseType next;
	BaseType forward; 
	if( pos >= list->capacity ) 
	{
		return 0;
	}
	if( (list != NULL) && (list->length > 0) )
	{
		current = list->heade->pxNext;
		for(i=0; i<pos; i++)
		{
			current = list->heade[current].pxNext;
		}
		
		item->itemVal = list->heade[current].empty = -1; //标记此区域可用 
		
		next = list->heade[current].pxNext; //获取当前数据的下一个 
		forward = list->heade[current].pxPre; //获取当前数据的前一个 
		
		list->heade[forward].pxNext = next; 
		list->heade[next].pxPre = forward;
		
		if(list->heade[current].pxNext == 0) //如果删除的是最后一个 
		{
			list->end = forward;
		}
		
		item->itemVal = list->heade[current].itemVal;
		item->value = list->heade[current].value;
		ret = 1;
		list->length--;
	}
	return ret;
}

/*
 * 获取列表中第一项的值，并删除
 * list：		从此列表获取
 * item：		用来存储返回的列表项
 * 返回0：失败	1：成功 
 */ 
uint8_t listPop(ListType* list, MiniItemType* item)
{
	return listDelete(list,item,0);
}

/*
 * 获取列表的大小 
 * list：		从此列表获取
 * 列表的大小 
 */ 
BaseType listCapacty(ListType* list)
{
	BaseType ret = 0;
	if(list != NULL)
	{
		ret = list->capacity;
	}
	return ret;
}

/*
 * 获取列表中列表项的个数 
 * list：		从此列表获取
 * 列表项的个数 
 */ 
BaseType listLength(ListType* list)
{
	BaseType ret = 0;
	if(list != NULL)
	{
		ret = list->length;
	}
	return ret;
}
