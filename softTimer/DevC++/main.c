#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "List.h"
#include "STimer.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void time1CallBackFun(TickType tick)
{
	printf("I'am run1 %d\r\n",tick);
}

void time2CallBackFun(TickType tick)
{
	printf("I'am run2 %d\r\n",tick);
}

void time4CallBackFun(TickType tick)
{
	printf("I'am run4 %d\r\n",tick);
}

void time5CallBackFun(TickType tick)
{
	printf("I'am run5 %d\r\n",tick);
}

void time6CallBackFun(TickType tick)
{
	printf("I'am run6 %d\r\n",tick);
}

void time7CallBackFun(TickType tick)
{
	printf("I'am run7 %d\r\n",tick);
}

void time8CallBackFun(TickType tick)
{
	printf("I'am run8 %d\r\n",tick);
}


int main(int argc, char *argv[]) {
	
	StimerIdType timer1,timer2,timer3,timer4,timer5,timer6,timer7,timer8;
	
	timer1 = STimer.create(500,time1CallBackFun,TIMER_OPT_REPEATE);
	timer2 = STimer.create(502,time2CallBackFun,TIMER_OPT_REPEATE);
	timer4 = STimer.create(501,time4CallBackFun,TIMER_OPT_REPEATE);
	timer5 = STimer.create(508,time5CallBackFun,TIMER_OPT_REPEATE);
	timer6 = STimer.create(504,time6CallBackFun,TIMER_OPT_REPEATE);
	timer7 = STimer.create(507,time7CallBackFun,TIMER_OPT_REPEATE);
	timer8 = STimer.create(505,time8CallBackFun,TIMER_OPT_REPEATE);
	
	STimer.delet(&timer4);

	timer3 = STimer.create(5000,NULL,TIMER_OPT_SINGLE);
	STimer.start(timer1);
	STimer.start(timer2);
	STimer.start(timer3);
	STimer.start(timer4);
	STimer.start(timer5);
	STimer.start(timer6);
	STimer.start(timer7);
	STimer.start(timer8);
	
	while(1)
	{
		timerTask();
		Sleep(1);
		if(STimer.state(timer3) == TIMER_STATE_COMPLETED)
		{
			break;
		}
	}
	return 0;
}

