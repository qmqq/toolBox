#ifndef __SOFT_TIMER_H_
#define __SOFT_TIMER_H_

#include "List.h"

#define SOFT_TIMER_MAX_MUN		(10)
#define TIMER_ERR				(0)
#define TIMER_TRUE				(1)

/*定时器状态定义*/ 
#define TIMER_STATE_UNUSED		(2) //刚创建的 
#define TIMER_STATE_RUNING		(3) //定时器正在运行 
#define TIMER_STATE_STOPPED		(4) //定时器停止 
#define TIMER_STATE_COMPLETED	(5) //定时器超时 
#define TIMER_STATE_DELETE		(6) //定时器被删除 
#define TIMER_STATE_NOW		    (7) // 

/*定时器选项*/
#define TIMER_OPT_SINGLE		(2) //定时器单次运行 
#define TIMER_OPT_REPEATE		(3) //定时器重复运行 

typedef void (*timerFun)(TickType tick);
typedef void* StimerIdType;	
typedef struct
{
	TickType	period; //定时器周期
	timerFun	callBack; //回调函数
	uint8_t		opt; //定时器功能选项
	uint8_t		state; //定时器状态
} SftTimerType;

typedef struct 
{
	StimerIdType (*create)(TickType period, timerFun callBack, uint8_t opt);
	uint8_t (*start)(StimerIdType id);
	uint8_t (*stop)(StimerIdType id);
	uint8_t (*delet)(StimerIdType *id);
	uint8_t (*state)(StimerIdType id);
	uint8_t (*period)(StimerIdType id);
	uint8_t (*opt)(StimerIdType id);
	TickType(*tick)(void);
	uint8_t (*setPerid)(StimerIdType id,TickType period);
	uint8_t (*setCallBack)(StimerIdType id,timerFun callBack);
	uint8_t (*setOpt)(StimerIdType id,uint8_t opt);
}__softTimerAllFun;

void timerTask(void);

/*STimer
 *软件定时器的所有操作注册在此结构体变量中 
 */
extern __softTimerAllFun STimer;
#endif /*__SOFT_TIMER_H_*/
