#include "uart.h"


static uint8_t uartTickCount = 0;
static uint8_t outTime = 0;

#define UART_TICK_CLEAR()	uartTickCount=0

void uartTickTask(void)	
{
	if(uartTickCount < outTime)
	{
		uartTickCount++;
	}
}

uint8_t uartIsFree(void)
{
	return (uartTickCount==outTime)? 1:0;
}

void uartOutTimeCalc(uint16_t baudRate)
{
	if(baudRate<= 9600)
	{
		outTime = 12000/baudRate;
		outTime = outTime*3;
	}
	else
	{
		outTime = 2;
	}
}

