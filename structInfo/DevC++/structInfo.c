#include <stdio.h>
#include <string.h>
#include "structInfo.h"

void structShowinit(char* msg,ShowStruct* _show, ChildInfo* info, unsigned char len)
{
	_show->msg = msg;
	_show->len = len;
	_show->arryInfo = info;
}

void structShow(ShowStruct* _show,void* source)
{
    unsigned char i,j;
    char buff[10];
    sprintf(buff,"%%-%ds",STRUCT_NAME_MAX_SIZE);
    char *p = source;
    printf("%s:>>>>>>\r\n",_show->msg);
    for(i = 0; i<_show->len; i++)
    {
        printf(buff,_show->arryInfo[i].name);
 		p = _show->arryInfo[i].addr;
        switch(_show->arryInfo[i]._type)
        {
            case __INT:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                {
                    printf("%d ",*(int*)p);
                    p +=4;
                }
                printf("\r\n");
                break;
            case __FLOAT:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                {
                    printf("%.4f ",*(float*)p);
                    p +=4;
                }
                printf("\r\n");
                break;
            case __CHAR:
                printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                {
                    printf("%c",*(char*)p);
                    p++;
                }
                 break;
            case __UCHAR:
            	printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                {
                    printf("%d",*(char*)p);
                    p++;
                }
                printf("\r\n");
                break;
            case __SHORT:
            	printf("=");
                for(j=0; j<_show->arryInfo[i].len; j++)
                {
                    printf("%d",*(char*)p);
                    p+=2;
                }
                printf("\r\n");
                break;
        }
    }
    printf("\r\n");
}
