#include <stdio.h>
#include <stdlib.h>
#include "structInfo.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct
{
	float staticPress;
	float dynamicPress;
	float atmosHeight;
	float TAS;
	float IAS;
	float hdot;	
}PressInfo;

PressInfo sPressInfo;

#define FATHER	sPressInfo
ChildInfo showInfoArry[6] = 
    {
    	AREA(staticPress,	__FLOAT,1),
    	AREA(dynamicPress,	__FLOAT,1),
    	AREA(atmosHeight,	__FLOAT,1),
    	AREA(TAS,			__FLOAT,1),
    	AREA(IAS,			__FLOAT,1),
    	AREA(hdot,			__FLOAT,1)
    };
#undef 	FATHER

char msg[] = "sPressInfo";
ShowStruct sPressInfoShow;

int main(int argc, char *argv[]) 
{
	structShowinit(msg,&sPressInfoShow, showInfoArry, 6);
	
	sPressInfo.staticPress = 599.5;
	sPressInfo.dynamicPress = 0.25;
	sPressInfo.atmosHeight = 12.0;
	sPressInfo.TAS = 6.7;
	sPressInfo.IAS = 6.9;
	sPressInfo.hdot = 498.4;
	
	structShow(&sPressInfoShow, &sPressInfo);
	return 0;
}
