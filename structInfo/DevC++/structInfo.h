#ifndef __STRUCT_INFO_
#define __STRUCT_INFO_

#define STRUCT_NAME_MAX_SIZE 	15
#define STRUCT_CLASS_MAX_NUM	20

typedef enum
{
	__UCHAR,
    __CHAR,
    __SHORT,
    __INT,
    __FLOAT,
    __STR,
}InfoType;


#define _STR(s)			(#s)
#define STR(s)			_STR(s)
#define _LINK(a,b)		(a##b)
#define LINK(a,b)		_LINK(a,b)

#define AREA(child,type,len)      {STR(child),&FATHER.child,type,len}

typedef struct
{
    char* 			name;
    void*			addr;
    InfoType 		_type;
    unsigned  char 	len;
}ChildInfo;

typedef struct
{
    char* 			msg;
    unsigned char 	len;
    ChildInfo*	arryInfo;
}ShowStruct;

void structShowinit(char* msg,ShowStruct* _show, ChildInfo* info, unsigned char len);
void structShow(ShowStruct* _show,void* source);
/*注意元素名不是字符串，而是变量名字 
#define FATHER	sPressInfo 	//填写结构体变量名 
ChildInfo showInfoArry[6] = 
    {
    	AREA(staticPress,	__FLOAT,1), //结构体元素名，元素类型，元素数组长度 （非数组则为1） 
    	AREA(dynamicPress,	__FLOAT,1),
    	AREA(atmosHeight,	__FLOAT,1),
    	AREA(TAS,			__FLOAT,1),
    	AREA(IAS,			__FLOAT,1),
    	AREA(hdot,			__FLOAT,1)
    };
#undef 	FATHER
char msg[] = "sPressInfo";
ShowStruct sPressInfoShow;
*/
#endif /*__STRUCT_INFO_*/
